﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// 枚举帮助类
    /// </summary>
    public class EnumHelper
    {
        #region 返回枚举对应属性

        /// <summary>
        /// 返回枚举对应属性
        /// </summary>
        /// <typeparam name="T">枚举类</typeparam>
        /// <returns></returns>
        public static List<Model.EnumType> EnumToList<T>()
        {
            List<Model.EnumType> list = new List<Model.EnumType>();

            var items = Enum.GetValues(typeof(T));
            foreach (var item in items)
            {
                Model.EnumType entity = new Model.EnumType();
                object[] obj = item.GetType().GetField(item.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (obj != null && obj.Length > 0)
                {
                    DescriptionAttribute da = obj[0] as DescriptionAttribute;
                    entity.Desction = da.Description;
                }

                entity.Value = Convert.ToInt32(item);
                entity.Name = item.ToString();
                list.Add(entity);
            }
            return list;
        }
        #endregion

        #region  获取枚举类型描述

        /// <summary>
        /// 获取枚举类型描述
        /// </summary>
        /// <param name="enumValue">枚举值</param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum enumValue)
        {
            string str = enumValue.ToString();
            System.Reflection.FieldInfo field = enumValue.GetType().GetField(str);
            object[] objs = field.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
            if (objs == null || objs.Length == 0) return str;
            System.ComponentModel.DescriptionAttribute da = (System.ComponentModel.DescriptionAttribute)objs[0];
            return da.Description;
        }
        #endregion

        #region 字符串转枚举

        /// <summary>
        /// 字符串转枚举
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <param name="value">名称</param>
        /// <returns></returns>
        public static T StringToEnum<T>(string value)
        {
            var obj = (T)Enum.Parse(typeof(T), value, false);
            return obj;
        }
        #endregion
    }
}
