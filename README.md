# CommonLib

#### 项目介绍
1. c# 常用类库整理 打包程序 
2. 主要是工作中经常使用的类库整理
3. 随时有新的类库,随时更新程序
4. qq群(811382380)

#### 安装教程
1. Nuget 直接搜索 : shuyu.common.lib
2. 软件采用.net 4.5 开发 

#### 目录说明
00. 000.Model.		本类库所所有实体
01. 001Time.		时间帮助类
02. 002.Cmd.		Cmd帮助类
03. 003.Cache		缓存帮助类,模拟session
04. 004.Http		http(post get pub delete)等操作
05. 005.File		file文件处理
06. 006.Ftp			ftp,平时用的比较少 未加入
07. 007.Enum		枚举帮助类
08. 008.DLL			未加入
09. 009.Serialize	序列化,反序列化
10. 010.Config		配置文件读取
11. 011.Image		图片处理
12. 012.DEncrypt	MD5加密没其他加密没加入
13. 013.List		集合处理
14. 014.Zip			压缩,解压缩
15. 015.Email		发送邮件
16. 016.Regex		正则表达式
17. 017.Rmb			人民币处理
18. 018.PinYin		拼音,支持多音字 比较强大
19. 019.Calendar	日历 后期加入
20. 020.Excel		Excel
21. 021.Tfs			Tfs二次开发,后期加入
22. 022.Windows IP	计算机名称等
23. 023.DataTable 
24. 024.String		字符串
25. 025.Cookie		Cookie帮助类
26. 026.DbHelper	数据库帮助类,支持简单事务处理,暂时只加入了mssqlserver
27. 027.LogHelper	日志帮助类
28. 028.Extended	扩展相关帮助类
29. 029.Lambda		Lambda帮助类
30. 030.WeiXin		微信公众号帮助类
31. 031.Entity		实体对象帮助类
32. 


#### 使用说明
1. 数据库dbHelper,默认形式
* var DbHelper = new CommonLib.DbHelper.Factory().IDBhelper;
* var Dt = DbHelper.ExecuteTable("select * from team_user");
2. 数据库dbHelper,自定义
* var DbHelper = new CommonLib.DbHelper.Factory() { Context = new CommonLib.Entity.DbContext() { ConnString = "ConnectionString1" } }.IDBhelper;
* var Dt = DbHelper.ExecuteTable("select * from team_user");
3. 获取数据库表结构(只支持sql server)
* var tableList = new CommonLib.DbHelper.Table("ConnectionString1").DicTable;