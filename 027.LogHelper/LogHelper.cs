﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// 日志帮助类
    /// </summary>
    public class LogHelper
    {
        /// <summary>
        /// 新增日志
        /// </summary>
        /// <param name="log">日志内容</param>
        /// <param name="type">日志类型</param>
        public static void AddLog(string log, CommonLib.Model.LogEnum type = CommonLib.Model.LogEnum.Info)
        {
            var fileName = DateTime.Now.ToString("yyyy-MM-dd") + ".log";

            log = string.Format("[{0}] {1}\r\n", DateTime.Now.ToString("HH:mm:ss fff"), log);
            var path = System.AppDomain.CurrentDomain.BaseDirectory + "Log\\" + type.ToString() + "\\" + fileName;
            CommonLib.FileHelper.FileWrite(path, log, true);
        }
    }
}
