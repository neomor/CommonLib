﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// LambdaHelper帮助类
    /// </summary>
    public class LambdaHelper<T>
    {
        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName == propertyValue
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Equal<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression left = Expression.PropertyOrField(parameter, name);
            ConstantExpression right = Expression.Constant(value, typeof(T1));//创建常数
            //return Expression.Equal(left, right);
            return Expression.Lambda<Func<T, bool>>(Expression.Equal(left, right), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName != propertyValue
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> NotEqual<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            ConstantExpression constant = Expression.Constant(value, typeof(T1));//创建常数 
            return Expression.Lambda<Func<T, bool>>(Expression.NotEqual(member, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName > propertyValue
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GreaterThan<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            ConstantExpression constant = Expression.Constant(value, typeof(T1));//创建常数 
            return Expression.Lambda<Func<T, bool>>(Expression.GreaterThan(member, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> LessThan<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            ConstantExpression constant = Expression.Constant(value, typeof(T1));//创建常数 
            return Expression.Lambda<Func<T, bool>>(Expression.LessThan(member, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName >= propertyValue
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GreaterThanOrEqual<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            ConstantExpression constant = Expression.Constant(value, typeof(T1));//创建常数 
            return Expression.Lambda<Func<T, bool>>(Expression.GreaterThanOrEqual(member, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName = propertyValue
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> LessThanOrEqual<T1>(string name, T1 value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            ConstantExpression constant = Expression.Constant(value, typeof(T1));//创建常数 
            return Expression.Lambda<Func<T, bool>>(Expression.LessThanOrEqual(member, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName.Contains(propertyValue)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> StartsWith(string name, string value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            MethodInfo method = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
            ConstantExpression constant = Expression.Constant(value, typeof(string));
            return Expression.Lambda<Func<T, bool>>(Expression.Call(member, method, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName.EndsWith(propertyValue)
        /// </summary> 
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> EndsWith(string name, string value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            MethodInfo method = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });
            ConstantExpression constant = Expression.Constant(value, typeof(string));
            return Expression.Lambda<Func<T, bool>>(Expression.Call(member, method, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：p=>p.propertyName.Contains(propertyValue)
        /// </summary> 
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Contains(string name, string value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            ConstantExpression constant = Expression.Constant(value, typeof(string));
            return Expression.Lambda<Func<T, bool>>(Expression.Call(member, method, constant), parameter);
        }

        /// <summary>
        /// 创建lambda表达式：!(p=>p.propertyName.Contains(propertyValue))
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> NotContains(string name, string value)
        {
            var parameter = Expression.Parameter(typeof(T), "ex");
            MemberExpression member = Expression.PropertyOrField(parameter, name);
            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            ConstantExpression constant = Expression.Constant(value, typeof(string));
            return Expression.Lambda<Func<T, bool>>(Expression.Not(Expression.Call(member, method, constant)), parameter);
        }
    }
}
