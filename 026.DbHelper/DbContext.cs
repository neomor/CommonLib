﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib.DbHelper
{
    /// <summary>
    /// 数据库上下文对象
    /// </summary>
    [Serializable]
    [DataContract]
    public class DbContext
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        [DataMember]
        public string ConnString { get; set; }

        /// <summary>
        /// 数据库类型 默认SqlServer 
        /// </summary>
        [DataMember]
        public SqlType? SqlType { get; set; }

        /// <summary>
        /// 数据库对象
        /// </summary>
        [DataMember]
        public SqlConnection Conn { get; set; }

        /// <summary>
        /// 事务对象
        /// </summary>
        [DataMember]
        public SqlTransaction Trans { get; set; } 
    }
}
