﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CommonLib.DbHelper
{
    /// <summary>
    /// 数据库帮助类 工厂入口
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// 上下文对象
        /// </summary>
        public DbHelper.DbContext Context { get; set; }

        /// <summary>
        /// 数据库初始化接口
        /// </summary>
        public IDBhelper IDBhelper
        {
            get
            {
                Init();
                switch (this.Context.SqlType)
                {
                    case SqlType.MsSqlServer:
                        {
                            var client = new MsSqlServer() { Context = this.Context };
                            return client;
                        }
                    case SqlType.MySql:
                    case SqlType.Oracle:
                    case SqlType.SQLite:
                    case SqlType.Odbc:
                        {
                            return null;
                        }
                    default:
                        {
                            return null;
                        }
                }
            }
        }

        private void Init()
        {
            if (this.Context == null)
            {
                this.Context = new CommonLib.DbHelper.DbContext();
            }
            if (string.IsNullOrEmpty(this.Context.ConnString))
            {
                this.Context.ConnString = "ConnectionString";
            }
            if (this.Context.SqlType == null)
            {
                this.Context.SqlType = SqlType.MsSqlServer;
            }
            this.Context.ConnString = ConfigHelper.GetConnectionStrings(this.Context.ConnString).ToString();
        }
    }
}
