﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CommonLib.DbHelper
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum SqlType
    {
        /// <summary>
        /// MsSqlServer
        /// </summary>
        [Description("MsSqlServer")]
        MsSqlServer,

        /// <summary>
        /// MySql
        /// </summary>
        [Description("MySql")]
        MySql,

        /// <summary>
        /// Oracle
        /// </summary>
        [Description("Oracle")]
        Oracle,

        /// <summary>
        /// SQLite
        /// </summary>
        [Description("SQLite")]
        SQLite,

        /// <summary>
        /// Odbc
        /// </summary>
        [Description("Odbc")]
        Odbc
    }
}
