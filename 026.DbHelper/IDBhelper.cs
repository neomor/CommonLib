﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CommonLib.DbHelper
{
    /// <summary>
    /// 数据库DB 帮助类接口
    /// </summary>
    public interface IDBhelper
    {
        ///// <summary>
        ///// 开始事务
        ///// </summary>
        //void BeginTransaction();

        ///// <summary>
        ///// 提交事务
        ///// </summary>
        //void Commit();

        ///// <summary>
        ///// 回滚事务
        ///// </summary>
        //void Rollback();


        #region IDataReader

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <returns>返回IDataReader</returns>
        IDataReader ExecuteReader(string sqlStr);

        /// <summary>
        /// 返回IDataReader
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回IDataReader</returns>
        IDataReader ExecuteReader(string sqlStr, object Params);

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 IDataReader类型的参数</returns>
        IDataReader ExecuteReader(string sqlStr, object[] Params);

        #endregion IDataReader

        #region DataTable

        /// <summary>
        /// 返回DataTable
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <returns></returns>
        DataTable ExecuteTable(string sqlStr);

        /// <summary>
        /// 返回 DataTable
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 DataTable类型的参数</returns>
        DataTable ExecuteTable(string sqlStr, object Params);

        /// <summary>
        /// 返回 DataTable
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 DataTable类型的参数</returns>
        DataTable ExecuteTable(string sqlStr, object[] Params);


        /// <summary>
        /// 分页存储过程 返回DataTable
        /// </summary>
        /// <param name="total">输出参数 总数量</param>
        /// <param name="tbName">表名(多表逗号隔开)</param>
        /// <param name="Fields">字段列表</param>
        /// <param name="OrderBy">排序</param>
        /// <param name="PageSize">每页数量</param>
        /// <param name="PageIndex">页码</param>
        /// <param name="Where">where条件</param>
        /// <param name="Params">参数</param>
        /// <returns></returns>
        DataTable ExecuteTablePage(ref int total, string tbName, string Fields, string OrderBy, int PageSize, int PageIndex, string Where, object[] Params);

        #endregion

        #region ExecuteScalar

        /// <summary>
        /// 返回sql语句的查询结果
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <returns>返回的是一个字符串</returns>
        string ExecuteScalar(string sqlStr);

        /// <summary>
        /// 返回 object
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 int类型的参数</returns>
        string ExecuteScalar(string sqlStr, object Params);

        /// <summary>
        /// 返回 object
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 int类型的参数</returns>
        string ExecuteScalar(string sqlStr, object[] Params);

        #endregion ExecuteScalar

        #region ExecuteScalars

        /// <summary>
        /// 返回查询的第一个字段值
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <returns></returns>
        string ExecuteScalars(string sqlStr);

        /// <summary>
        /// 返回查询的第一个字段值,逗号隔开
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">参数列表</param>
        /// <returns></returns>
        string ExecuteScalars(string sqlStr, object Params);

        /// <summary>
        /// 返回查询的第一个字段值,逗号隔开
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">参数列表</param>
        /// <returns></returns>
        string ExecuteScalars(string sqlStr, object[] Params);

        #endregion ExecuteScalars

        #region ExecuteNonQuery

        /// <summary>
        /// 执行ExecuteNonQuery
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <returns></returns>
        int ExecuteNonQuery(string sqlStr);


        /// <summary>
        /// 执行ExecuteNonQuery
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 int类型的参数</returns>
        int ExecuteNonQuery(string sqlStr, object Params);

        /// <summary>
        /// 执行 ExecuteNonQuery
        /// </summary>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="Params">object参数数组</param>
        /// <returns>返回值 int类型的参数</returns>
        int ExecuteNonQuery(string sqlStr, object[] Params);

        #endregion ExecuteNonQuery  

        #region 添加SqlParameter参数
        /// <summary>
        /// 添加 mssql 参数话查询 参数
        /// </summary>
        /// <param name="ParamName">参数名 @名称 </param>
        /// <param name="sqlType">参数类型</param>
        /// <param name="Size">参数的长度</param>
        /// <param name="value">参数的值</param>
        /// <returns>返回 SqlParameter 对象</returns>
        DbParameter AddParam(string ParamName, SqlDbType sqlType, int Size, object value);

        /// <summary>
        /// 添加 mssql 参数话查询 参数
        /// </summary>
        /// <param name="ParamName">参数名 @名称</param>
        /// <param name="value">参数的值</param>
        /// <returns>返回 SqlParameter 对象</returns>
        DbParameter AddParam(string ParamName, object value);

        #endregion 添加SqlParameter参数
    }
}
