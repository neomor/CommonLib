﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2019-05-09 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// 钉钉帮助类
    /// </summary>
    public class DingDing
    {
        #region 发送钉钉群消息

        /// <summary>
        /// 发送钉钉群消息
        /// </summary>
        /// <param name="url"></param>
        /// <param name="content">内容</param>
        /// <param name="mobiles">@人的手机号</param>
        /// <param name="isAtAll">是否@所有人 默认false</param>
        public static void SendWebhook(string url, string content, List<string> mobiles, bool isAtAll = false)
        {
            var obj = new
            {
                msgtype = "text",
                text = new
                {
                    content = content
                },
                at = new
                {
                    atMobiles = mobiles,
                    isAtAll = isAtAll
                }
            };
            var str = JsonConvert.SerializeObject(obj);
            var res = HttpHelper.HttpPost(url, str);
        }

        /// <summary>
        /// 发送钉钉群消息
        /// </summary>
        /// <param name="content"></param>
        /// <param name="isAtAll"></param>

        public static void SendDingDing(string url, string content, bool isAtAll = false)
        {
            SendWebhook(ConfigHelper.GetAppSettings("WebhookUrl"), content, new List<string>() { }, isAtAll);
        } 
        #endregion
    }
}
