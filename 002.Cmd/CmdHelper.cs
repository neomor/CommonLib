﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// cmd帮助类
    /// </summary>
    public class CmdHelper
    {
        /// <summary>
        /// 执行Cmd命令
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static string Exec(string cmd)
        {
            var proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";

            proc.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            proc.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            proc.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            proc.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            proc.StartInfo.CreateNoWindow = true;//不显示程序窗口
            proc.Start();//启动程序 

            if (!string.IsNullOrEmpty(cmd))
            {
                proc.StandardInput.WriteLine(cmd);
            }

            proc.StandardInput.WriteLine("exit");
            var result = proc.StandardOutput.ReadToEnd();

            proc.WaitForExit();
            proc.Close();

            return result;
        }

        /// <summary>
        /// 执行Cmd命令
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string Exec(List<string> list)
        {
            var proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";

            proc.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            proc.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            proc.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            proc.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            proc.StartInfo.CreateNoWindow = true;//不显示程序窗口
            proc.Start();//启动程序 

            foreach (var item in list)
            {
                proc.StandardInput.WriteLine(item);
            }

            proc.StandardInput.WriteLine("exit");
            var result = proc.StandardOutput.ReadToEnd();

            proc.WaitForExit();
            proc.Close();

            return result;
        }
    }
}
