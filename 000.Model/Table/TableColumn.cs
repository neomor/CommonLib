﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib.Model
{
    [Serializable]
    [DataContract]
    public class TableColumn
    {
        /// <summary>
        /// 表名称
        /// </summary>
        [DataMember]
        public string TableName { get; set; }

        /// <summary>
        /// 表说明
        /// </summary>
        [DataMember]
        public string TableComments { get; set; }

        /// <summary>
        /// 列名称
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// 列说明
        /// </summary>
        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// 字段长度
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// 是否为空
        /// </summary>
        [DataMember]
        public bool IsNull { get; set; }

        /// <summary>
        /// 是否主键
        /// </summary>
        [DataMember]
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 是否标识列
        /// </summary>
        [DataMember]
        public bool IsIdentity { get; set; }
    }
}
