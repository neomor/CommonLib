﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib.Model
{
    /// <summary>
    /// 表
    /// </summary> 
    [Serializable]
    [DataContract]
    public class Table
    {
        /// <summary>
        /// 表名称
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// 表说明
        /// </summary>
        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// 表主键名称
        /// </summary>
        [DataMember]
        public string PrimaryName { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        [DataMember]
        public List<TableColumn> Columns { get; set; } 
    }
}
