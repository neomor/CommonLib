﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Model
{ 
    /// <summary>
    /// 日志枚举
    /// </summary>
    public enum LogEnum
    {
        /// <summary>
        /// 默认日志
        /// </summary>
        [Description("默认日志")]
        Info,

        /// <summary>
        /// 错误日志
        /// </summary>
        [Description("错误日志")]
        Error,

        /// <summary>
        /// 警告日志
        /// </summary>
        [Description("警告日志")]
        Warning
    }
}
