﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Model
{
    /// <summary>
    /// 分页
    /// </summary>
    [Serializable]
    [DataContract]
    public class Pages
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public Pages()
        {
            if (this.pageSize == 0)
            {
                this.pageSize = 15;
            }
            if (this.pageNumber == 0)
            {
                this.pageNumber = 1;
            }
        }

        /// <summary>
        /// 每页行数
        /// </summary>
        [DataMember]
        public int pageSize { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        [DataMember]
        public int pageNumber { get; set; }

        /// <summary>
        /// 排序列 字段名称
        /// </summary>
        [DataMember]
        public string sort { get; set; }

        /// <summary>
        /// 排序类型 asc desc
        /// </summary>
        [DataMember]
        public string order { get; set; }

        /// <summary>
        /// 总记录数 只读
        /// </summary>
        [DataMember]
        public int total { get; set; }

        /// <summary>
        /// 总页数 只读参数
        /// </summary>
        [DataMember]
        public int pages
        {
            get
            {
                if (total > 0)
                {
                    return total % this.pageSize == 0 ? total / this.pageSize : total / this.pageSize + 1;
                }
                else
                {
                    return 1;
                }
            }
        }

        /// <summary>
        /// 是否是最后一页 只读参数
        /// </summary>
        [DataMember]
        public bool lastPage
        {
            get
            {
                return (pageNumber == pages) ? true : false;
            }
        }
    }
}
