﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Model
{
    /// <summary>
    /// 拼音
    /// </summary>
    public class PinYin
    {
        /// <summary>
        /// 
        /// </summary>
        public PinYin()
        {
            PingYin = new List<string>();
            FirstPingYin = new List<string>();
        }

        /// <summary>
        /// 全部拼音
        /// </summary>
        public List<string> PingYin { get; set; }

        /// <summary>
        /// 首字母
        /// </summary>
        public List<string> FirstPingYin { get; set; }
    }
}
