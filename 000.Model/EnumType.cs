﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Model
{
    /// <summary>
    /// 枚举类型
    /// </summary>
    [Serializable]
    [DataContract]
    public class EnumType
    {
        /// <summary>  
        /// 枚举对象的值  
        /// </summary>  
        [DataMember]
        public int Value { set; get; }

        /// <summary>  
        /// 名称  
        /// </summary>  
        [DataMember]
        public string Name { set; get; }

        /// <summary>  
        /// 描述  
        /// </summary>  
        [DataMember]
        public string Desction { set; get; }
    }
}
