﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CommonLib.WeiXin
{
    public class Info
    {
        private string appID = "";
        private string appSecret = "";
        private string url = "";
        private string token = "";
        private string access_token = "";

        public string AppID
        {
            get { return this.appID; }
            set { this.appID = value; }
        }
        public string AppSecret
        {
            get { return this.appSecret; }
            set { this.appSecret = value; }
        }
        public string Url
        {
            get { return this.url; }
            set { this.url = value; }
        }
        public string Token
        {
            get { return this.token; }
            set { this.token = value; }
        }
        public string Access_token
        {
            get { return this.access_token; }
            set { this.access_token = value; }
        }

        public Info(string guid)
        {
            var DbHelper = new CommonLib.DbHelper.Factory().IDBhelper;
            var sdr = DbHelper.ExecuteReader("select 微信号, APPID, AppSecret, access_token, access_token_time, url, token, 原始id from 微信 where 标识=@标识", new SqlParameter("@标识", guid));
            if (sdr.Read())
            {
                appID = sdr["APPID"].ToString();
                appSecret = sdr["AppSecret"].ToString();
                url = sdr["url"].ToString();
                token = sdr["token"].ToString();

                //更新 access_token access_token_time
                TimeSpan ts = DateTime.Now - Convert.ToDateTime(sdr["access_token_time"].ToString());
                if (sdr["access_token_time"].ToString() == "")
                {
                    access_token = Return_access_token(appID, appSecret);

                    SqlParameter[] P = { new SqlParameter("@access_token", access_token), new SqlParameter("@access_token_time", DateTime.Now), new SqlParameter("@标识", guid) };
                    DbHelper.ExecuteNonQuery("UPDATE 微信 SET access_token = @access_token, access_token_time = @access_token_time WHERE (标识 = @标识)", P);
                }
                else if (ts.TotalSeconds > 7200)//access_token 有效期 7200秒
                {
                    access_token = Return_access_token(appID, appSecret);

                    SqlParameter[] P = { new SqlParameter("@access_token", access_token), new SqlParameter("@access_token_time", DateTime.Now), new SqlParameter("@标识", guid) };
                    DbHelper.ExecuteNonQuery("UPDATE 微信 SET access_token = @access_token, access_token_time = @access_token_time WHERE (标识 = @标识)", P);
                }
                else
                {
                    access_token = sdr["access_token"].ToString();
                }
            }
            sdr.Close();
        }

        private string Return_access_token(string appID, string appsecret)
        {
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appID + "&secret=" + appsecret;
            string json = CommonLib.HttpHelper.HttpGet(url, "utf-8");

            string[] item = json.Split(',');
            item = item[0].Split(':');
            return item[1].Replace("\"", "");
        }
    }
}
