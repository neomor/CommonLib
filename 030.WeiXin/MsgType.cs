﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.WeiXin
{
    /// <summary>
    /// MsgType 枚举类型
    /// </summary>
    public enum MsgType
    {
        /// <summary>
        /// 文本消息
        /// </summary>
        text,
        /// <summary>
        /// 图片消息
        /// </summary>
        image,
        /// <summary>
        /// 语音消息
        /// </summary>
        voice,
        /// <summary>
        /// 视频消息
        /// </summary>
        video,
        /// <summary>
        /// 音乐消息
        /// </summary>
        music,
        /// <summary>
        /// 图文消息
        /// </summary>
        news,
        /// <summary>
        /// 地理位置消息
        /// </summary>
        location,
        /// <summary>
        /// 链接消息
        /// </summary>
        link,
        /// <summary>
        /// 推送事件
        /// </summary>
        Event
    }
}
