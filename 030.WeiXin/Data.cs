﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLib.WeiXin
{
    /// <summary>
    /// 存储微信数据
    /// </summary>
    public class Data
    {
        private string toUserName = "";
        private string fromUserName = "";

        private string createTime = "";
        private string msgType = "";
        private string content = "";
        private string msgId = "";

        private string picUrl = "";
        private string mediaId = "";
        private string format = "";
        private string recognition = "";

        private string thumbMediaId = "";

        private string label = "";
        private string location_X = "";
        private string location_Y = "";
        private string scale = "";

        private string title = "";
        private string url = "";
        private string description = "";

        private string _event = "";
        private string eventKey = "";

        private string postXml = "";

        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName
        {
            get { return this.toUserName; }
            set { this.toUserName = value; }
        }
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName
        {
            get { return this.fromUserName; }
            set { this.fromUserName = value; }
        }
        /// <summary>
        /// 消息创建时间 （整型）
        /// </summary>
        public string CreateTime
        {
            get { return this.createTime; }
            set { this.createTime = value; }
        }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string MsgType
        {
            get { return this.msgType; }
            set { this.msgType = value; }
        }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content
        {
            get { return this.content; }
            set { this.content = value; }
        }
        /// <summary>
        /// 消息id，64位整型
        /// </summary>
        public string MsgId
        {
            get { return this.msgId; }
            set { this.msgId = value; }
        }
        /// <summary>
        /// 图片链接地址
        /// </summary>
        public string PicUrl
        {
            get { return this.picUrl; }
            set { this.picUrl = value; }
        }
        /// <summary>
        /// 图片/语音/视频 消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string MediaId
        {
            get { return this.mediaId; }
            set { this.mediaId = value; }
        }

        /// <summary>
        /// 语音格式，如amr，speex等
        /// </summary>
        public string Format
        {
            get { return this.format; }
            set { this.format = value; }
        }

        /// <summary>
        /// 语音识别结果，UTF8编码
        /// </summary>
        public string Recognition
        {
            get { return this.recognition; }
            set { this.recognition = value; }
        }
        /// <summary>
        /// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string ThumbMediaId
        {
            get { return this.thumbMediaId; }
            set { this.thumbMediaId = value; }
        }

        /// <summary>
        /// 地理位置信息
        /// </summary>
        public string Label
        {
            get { return this.label; }
            set { this.label = value; }
        }
        /// <summary>
        /// 地理位置维度
        /// </summary>
        public string Location_X
        {
            get { return this.location_X; }
            set { this.location_X = value; }
        }
        /// <summary>
        /// 地理位置经度
        /// </summary>
        public string Location_Y
        {
            get { return this.location_Y; }
            set { this.location_Y = value; }
        }
        /// <summary>
        /// 地图缩放大小
        /// </summary>
        public string Scale
        {
            get { return this.scale; }
            set { this.scale = value; }
        }



        /// <summary>
        /// 消息标题
        /// </summary>
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }
        /// <summary>
        /// 消息链接
        /// </summary>
        public string Url
        {
            get { return this.url; }
            set { this.url = value; }
        }
        /// <summary>
        /// 消息描述
        /// </summary>
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }


        /// <summary>
        /// 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
        /// </summary>
        public string Event
        {
            get { return this._event; }
            set { this._event = value; }
        }
        /// <summary>
        /// 事件KEY值，与自定义菜单接口中KEY值对应
        /// </summary>
        public string EventKey
        {
            get { return this.eventKey; }
            set { this.eventKey = value; }
        }
        /// <summary>
        /// 接收的xml
        /// </summary>
        public string PostXml
        {
            get { return this.postXml; }
            set { this.postXml = value; }
        }

    }
}
