﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2019-05-09 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLib.Extended
{
    /// <summary>
    /// 对象非空验证 扩展
    /// </summary>
    public static class IsNullOrEmptyExtended
    {
        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param> 
        /// <returns></returns>
        public static bool ExIsNullOrEmpty<T>(this T obj)
        {
            var flag = true;
            if (obj != null)
            {
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool ExIsNullOrEmpty(this DateTime obj)
        {
            var flag = true;
            if (obj != null)
            {
                if (obj == DateTime.MinValue)
                {
                    flag = false;
                }
            }
            return flag;
        }

        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool ExIsNullOrEmpty(this DateTime? obj)
        {
            if (obj == null)
            {
                return true;
            }
            return obj.Value.ExIsNullOrEmpty();
        }

        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool ExIsNullOrEmpty(this string obj)
        {
            var flag = true;
            if (obj != null)
            {
                if (obj == string.Empty)
                {
                    flag = false;
                }
            }
            return flag;
        }

        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool ExIsNullOrEmpty(this Guid obj)
        {
            var flag = true;
            if (obj != null)
            {
                if (obj != Guid.Empty)
                {
                    flag = false;
                }
            }
            return flag;
        }

        /// <summary>
        /// 非空验证
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool ExIsNullOrEmpty(this Guid? obj)
        {
            if (obj == null)
            {
                return true;
            }
            return obj.Value.ExIsNullOrEmpty();
        }
    }
}
