﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Extended
{
    /// <summary>
    /// 实体对象扩展
    /// </summary>
    public static class ConvertExtended
    {
        /// <summary>
        /// 转换成long类型
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static long ExToLong(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return long.Parse(str);
            }
            return default(long);
        }

        /// <summary>
        /// 转换成int整形
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static long ExToInt(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return int.Parse(str);
            }
            return default(int);
        }

    }
}
