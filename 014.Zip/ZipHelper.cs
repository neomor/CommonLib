﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// Zip压缩解压缩 帮助类
    /// </summary>
    public class ZipHelper
    {
        #region ZipFile 压缩文件

        /// <summary>
        /// 压缩
        /// </summary>
        /// <param name="strDirectory">要压缩的目录</param>
        /// <param name="zipFile">压缩后的zip地址 绝对地址</param>
        public static void ZipFileDirectory(string strDirectory, string zipFile)
        {
            if (File.Exists(zipFile))
            {
                File.Delete(zipFile);
            }
            System.IO.Compression.ZipFile.CreateFromDirectory(strDirectory, zipFile);//压缩 
        }

        /// <summary>
        /// 压缩文件列表
        /// </summary>
        /// <param name="files">要压缩的文件集合</param>
        /// <param name="zipFile">压缩后的zip地址 绝对地址</param>
        public static void ZipFiles(List<string> files, string zipFile)
        {
            if (File.Exists(zipFile))
            {
                File.Delete(zipFile);
            }
            using (ZipArchive archive = ZipFile.Open(zipFile, ZipArchiveMode.Create))
            {
                foreach (var item in files)
                {
                    if (File.Exists(item))
                    {
                        archive.CreateEntryFromFile(item, Path.GetFileName(item));
                    }
                }
            }
        }
        #endregion

        #region ZipFile 解压文件

        /// <summary>
        /// 解压缩
        /// </summary>
        /// <param name="zipFile">zip地址</param>
        /// <param name="strDirectory">解压</param>
        /// <param name="overWrite">是否覆盖已存在文件</param>
        public static void UnZip(string zipFile, string strDirectory, bool overWrite = true)
        {
            using (var zip = System.IO.Compression.ZipFile.OpenRead(zipFile))
            {
                foreach (var item in zip.Entries)
                {
                    var fileName = Path.Combine(strDirectory, item.FullName);
                    var fileDir = Path.GetDirectoryName(fileName);

                    if (!Directory.Exists(fileDir))
                    {
                        Directory.CreateDirectory(fileDir);
                    }
                    try
                    {
                        item.ExtractToFile(fileName, overWrite);
                    }
                    catch { }
                }
            }
        }
        #endregion
    }
}
