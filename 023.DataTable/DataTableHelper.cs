﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// DataTable 帮助类
    /// </summary>
    public class DataTableHelper
    {
        #region DataTable转IList

        /// <summary>
        /// DataTable转IList集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public static IList<T> ToList<T>(DataTable dataTable) where T : class, new()
        {
            IList<T> list = new List<T>();// 定义集合
            if (dataTable != null)
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    T t = new T();
                    PropertyInfo[] propertys = t.GetType().GetProperties();// 获得此模型的公共属性
                    foreach (PropertyInfo pi in propertys)
                    {
                        var name = pi.Name;
                        if (dataTable.Columns.Contains(name))
                        {
                            if (!pi.CanWrite) continue;
                            object value = dr[name];
                            if (value != DBNull.Value)
                            {
                                pi.SetValue(t, value, null);
                            }
                        }
                    }
                    list.Add(t);
                }
            }
            return list;
        }
        #endregion
    }
}
