﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CommonLib
{
    /// <summary>
    /// 未分类工具
    /// </summary>
    public class Tools
    {
        #region Base64SaveImage 将Base64字符串转换为图片

        /// <summary>
        /// 将Base64字符串转换为图片
        /// </summary>
        /// <param name="Base64Str"></param> 
        /// <returns></returns>
        public static string Base64SaveImage(string Base64Str)
        {
            try
            {
                var url = string.Format("{0}{1}", FileHelper.SavedFolder(), Guid.NewGuid() + ".jpg");

                var localPath = HttpContext.Current.Server.MapPath(url);
                if (!Directory.Exists(Path.GetDirectoryName(localPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(localPath));
                }

                byte[] arr = Convert.FromBase64String(Base64Str);
                MemoryStream ms = new MemoryStream(arr);
                Bitmap bmp = new Bitmap(ms);
                bmp.Save(localPath, System.Drawing.Imaging.ImageFormat.Jpeg);   //保存为.jpg格式
                ms.Close();

                //ManagerUtilities.Images.SaveThumbnailImage(localPath, localPath + "_tbi2.jpg", 400, 0, 96);
                return url;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                //ManagerUtilities.TxtHelper.txt_writer("/err/" + DateTime.Now.ToString("yyyy-MM-dd-HH-1") + ".txt", ex.Message + "\r\n", "utf-8");
            }
            return "";
        }
        #endregion

        #region 计算2个点经纬度之间的距离

        /// <summary>
        /// 给定的经度1，纬度1；经度2，纬度2. 计算2个经纬度之间的距离。
        /// </summary>
        /// <param name="lat1">经度1(-180--180)</param>
        /// <param name="lon1">纬度1(-90--90)</param>
        /// <param name="lat2">经度2</param>
        /// <param name="lon2">纬度2</param>
        /// <returns>距离 米</returns>
        public static double Distance(double lat1, double lon1, double lat2, double lon2)
        {
            //用haversine公式计算球面两点间的距离。
            //经纬度转换成弧度
            lat1 = ConvertDegreesToRadians(lat1);
            lon1 = ConvertDegreesToRadians(lon1);
            lat2 = ConvertDegreesToRadians(lat2);
            lon2 = ConvertDegreesToRadians(lon2);

            //差值
            var vLon = Math.Abs(lon1 - lon2);
            var vLat = Math.Abs(lat1 - lat2);

            //h is the great circle distance in radians, great circle就是一个球体上的切面，它的圆心即是球心的一个周长最大的圆。
            var h = HaverSin(vLat) + Math.Cos(lat1) * Math.Cos(lat2) * HaverSin(vLon);

            double EARTH_RADIUS = 6378.137;//km 地球半径 平均值，千米
            var distance = 2 * EARTH_RADIUS * Math.Asin(Math.Sqrt(h));

            return distance * 1000;
        }

        private static double HaverSin(double theta)
        {
            var v = Math.Sin(theta / 2);
            return v * v;
        }
        /// <summary>
        /// 将角度换算为弧度。
        /// </summary>
        /// <param name="degrees">角度</param>
        /// <returns>弧度</returns>
        private static double ConvertDegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }
        private static double ConvertRadiansToDegrees(double radian)
        {
            return radian * 180.0 / Math.PI;
        }
        #endregion
    }
}
