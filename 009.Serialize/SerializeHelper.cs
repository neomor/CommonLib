﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// 实体对象 序列化 帮助类
    /// </summary>
    public class SerializeHelper
    {
        /// <summary>
        /// 序列化 实体->字符串
        /// </summary>
        /// <returns></returns>
        public static string SerializeObject(object obj)
        {
            var str = JsonConvert.SerializeObject(obj);
            return str;
        }

        /// <summary>
        /// 反序列化 字符串->实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string str) where T : class, new()
        {
            var obj = JsonConvert.DeserializeObject<T>(str);
            return obj;
        }
    }
}
