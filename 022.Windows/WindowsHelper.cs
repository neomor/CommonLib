﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// 系统级帮助类
    /// </summary>
    public class WindowsHelper
    {
        #region 获取多屏幕中最大分辨率屏幕 

        /// <summary>
        /// 获取多屏幕中最大分辨率屏幕
        /// </summary>
        public static System.Windows.Forms.Screen GetMaxScreen()
        {
            var index = 0;
            var items = System.Windows.Forms.Screen.AllScreens;
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].Bounds.Width > items[i].Bounds.Height)
                {
                    index = i;
                }
            }
            return items[index];
        }
        #endregion

        #region 获取防火墙状态

        /// <summary>
        /// 获取防火墙状态
        /// </summary>
        /// <returns>true-打开 false-关闭</returns>
        public static bool GetFirewallState()
        {
            const string CLSID_FIREWALL_MANAGER = "{304CE942-6E39-40D8-943A-B913C40C9CD4}";
            Type objType = Type.GetTypeFromCLSID(new Guid(CLSID_FIREWALL_MANAGER));
            var netFwMgr = Activator.CreateInstance(objType) as NetFwTypeLib.INetFwMgr;
            var state = netFwMgr.LocalPolicy.CurrentProfile.FirewallEnabled;

            return state;
        }
        #endregion

        #region 获取本地计算机的主机名

        /// <summary>
        /// 获取本地计算机的主机名
        /// </summary>
        /// <returns></returns>
        public static string GetHostName()
        {
            var hostName = Dns.GetHostName();
            return hostName;
        }
        #endregion

        #region 获取本级所有IP4地址

        /// <summary>
        /// 获取本级所有IP4地址
        /// </summary>
        /// <returns></returns>
        public static List<string> GetHostIP(string ipbeginwith = "")
        {
            var hostName = Dns.GetHostName();
            var ipe = Dns.GetHostEntry(hostName);

            var list = ipe.AddressList.Where(ex => ex.AddressFamily == AddressFamily.InterNetwork).Select(ex => ex.ToString()).ToList();
            return list;
        }
        #endregion
        
        #region 浏览器打开指定地址

        /// <summary>
        /// 浏览器打开指定地址
        /// </summary>
        /// <param name="url"></param>
        public static void OpenUrl(string url)
        { 
            System.Diagnostics.Process.Start(url);
        }
        #endregion
    }
}
