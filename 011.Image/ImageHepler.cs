﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CommonLib
{
    /// <summary>
    /// 图片操作帮助类 方法不太好 需要后期调整优化
    /// </summary>
    public class ImageHepler
    {
        private static System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            int j;
            System.Drawing.Imaging.ImageCodecInfo[] encoders;
            encoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        /// <summary>
        /// 生成缩略图(仅限JPG图片)
        /// </summary>
        /// <param name="ImgFileName">原JPG图片路径名</param>
        /// <param name="ImgWidth">缩略图宽度,指定为0时按高度等比例缩放</param> 
        /// <param name="JpgLevel">缩略图压缩品质</param>
        /// <param name="SaveFileName">保存JPG文件路径名</param>
        public static void SaveThumbnailImage(string ImgFileName, string SaveFileName, int ImgWidth, long JpgLevel = 85)
        {
            var ImgHeight = 0;
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(ImgFileName)))
            {
                //生成缩略图
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(ImgFileName));

                //原图尺寸
                int oWidth = myImage.Width;
                int oHeight = myImage.Height;

                //新尺寸
                int toWidth = ImgWidth;
                int toHeight = ImgHeight;

                //指定宽度按高比例缩放
                if (ImgWidth > 0 && ImgHeight == 0)
                {
                    toWidth = ImgWidth;
                    toHeight = oHeight * ImgWidth / oWidth;
                }

                //指定高度按宽比例缩放
                if (ImgHeight > 0 && ImgWidth == 0)
                {
                    toHeight = ImgHeight;
                    toWidth = oWidth * ImgHeight / oHeight;
                }

                //如果高宽均为0给默认大小
                if (ImgHeight == 0 && ImgWidth == 0)
                {
                    toWidth = 128;
                    toHeight = 96;
                }

                System.Drawing.Bitmap myBitmap = new System.Drawing.Bitmap(toWidth, toHeight);
                System.Drawing.Graphics myGraphics = System.Drawing.Graphics.FromImage(myBitmap);

                //高质量重新绘制缩略图
                myGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                myGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                myGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                myGraphics.DrawImage(myImage, new System.Drawing.Rectangle(0, 0, toWidth, toHeight), 0, 0, oWidth, oHeight, System.Drawing.GraphicsUnit.Pixel);
                myGraphics.Dispose();

                //直接获取缩略图
                //myBitmap = new System.Drawing.Bitmap(myImage.GetThumbnailImage(toWidth, toHeight, null, IntPtr.Zero));

                myImage.Dispose();

                System.Drawing.Imaging.ImageCodecInfo myImageCodecInfo;
                System.Drawing.Imaging.Encoder myEncoder;
                System.Drawing.Imaging.EncoderParameter myEncoderParameter;
                System.Drawing.Imaging.EncoderParameters myEncoderParameters;

                myImageCodecInfo = GetEncoderInfo("image/jpeg");
                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, JpgLevel);
                myEncoderParameters.Param[0] = myEncoderParameter;

                ////如果图片存在先删除图片
                //if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(SaveFileName)))
                //{
                //    System.IO.File.Delete(HttpContext.Current.Server.MapPath(SaveFileName));
                //}
                myBitmap.Save(HttpContext.Current.Server.MapPath(SaveFileName), myImageCodecInfo, myEncoderParameters);
                myBitmap.Dispose();
            }
        }


        /// <summary>
        /// 设置文字水印并保存图片(仅限JPG图片)
        /// </summary>
        /// <param name="ImgFileName">原图片</param>
        /// <param name="wmText">水印文字</param>
        /// <param name="wmFont">文字格式</param>
        /// <param name="wmColor">颜色</param>
        /// <param name="wmLeft">左边距</param>
        /// <param name="wmTop">上边距</param>
        /// <param name="JpgLevel">压缩品质</param>
        public static void SetWaterMarkText(string ImgFileName, string wmText, System.Drawing.Font wmFont, System.Drawing.Color wmColor, float wmLeft, float wmTop, long JpgLevel)
        {
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(ImgFileName)))
            {
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(ImgFileName));

                System.Drawing.Graphics myGraphics = System.Drawing.Graphics.FromImage(myImage);
                myGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                myGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                myGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                myGraphics.DrawImage(myImage, 0, 0, myImage.Width, myImage.Height);
                System.Drawing.Brush wmBrush = new System.Drawing.SolidBrush(wmColor);

                myGraphics.DrawString(wmText, wmFont, wmBrush, wmLeft, wmTop);
                myGraphics.Dispose();

                System.Drawing.Imaging.ImageCodecInfo myImageCodecInfo;
                System.Drawing.Imaging.Encoder myEncoder;
                System.Drawing.Imaging.EncoderParameter myEncoderParameter;
                System.Drawing.Imaging.EncoderParameters myEncoderParameters;

                myImageCodecInfo = GetEncoderInfo("image/jpeg");
                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, JpgLevel);
                myEncoderParameters.Param[0] = myEncoderParameter;

                System.Drawing.Bitmap myBitmap;
                myBitmap = new System.Drawing.Bitmap(myImage);
                myImage.Dispose();
                myBitmap.Save(HttpContext.Current.Server.MapPath(ImgFileName), myImageCodecInfo, myEncoderParameters);
                myBitmap.Dispose();

            }
        }

        /// <summary>
        /// 设置图片水印并保存图片(仅限JPG图片)
        /// </summary>
        /// <param name="ImgFileName">原图片</param>
        /// <param name="wmImgFileName">水印图片</param>
        /// <param name="wmLeft">左边距,如果为负数,以右边距计算</param>
        /// <param name="wmTop">上边距,如果为负数,以下边距计算</param>
        /// <param name="wmAlpha">水印透明度</param>
        /// <param name="JpgLevel">压缩品质</param>
        public static void SetWaterMarkImage(string ImgFileName, string wmImgFileName, int wmLeft, int wmTop, int wmAlpha, long JpgLevel)
        {
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(ImgFileName)) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(wmImgFileName)))
            {
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(ImgFileName));
                System.Drawing.Image copyImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(wmImgFileName));

                //如果是负数以右下角为基准计算
                int toLeft = wmLeft;
                int toTop = wmTop;
                if (wmLeft < 0) toLeft = myImage.Width - (copyImage.Width - wmLeft);
                if (wmTop < 0) toTop = myImage.Height - (copyImage.Height - wmTop);

                System.Drawing.Graphics myGraphics = System.Drawing.Graphics.FromImage(myImage);
                myGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                myGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                myGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                //设置颜色矩阵
                float[][] matrixItems ={
                                       new float[] {1, 0, 0, 0, 0},
                                       new float[] {0, 1, 0, 0, 0},
                                       new float[] {0, 0, 1, 0, 0},
                                       new float[] {0, 0, 0, (float)wmAlpha/100f, 0},
                                       new float[] {0, 0, 0, 0, 1}
                                       };

                System.Drawing.Imaging.ColorMatrix colorMatrix = new System.Drawing.Imaging.ColorMatrix(matrixItems);
                System.Drawing.Imaging.ImageAttributes myImageAttributes = new System.Drawing.Imaging.ImageAttributes();
                myImageAttributes.SetColorMatrix(colorMatrix, System.Drawing.Imaging.ColorMatrixFlag.Default, System.Drawing.Imaging.ColorAdjustType.Bitmap);

                myGraphics.DrawImage(copyImage, new System.Drawing.Rectangle(toLeft, toTop, copyImage.Width, copyImage.Height), 0, 0, copyImage.Width, copyImage.Height, System.Drawing.GraphicsUnit.Pixel, myImageAttributes);
                myGraphics.Dispose();

                System.Drawing.Imaging.ImageCodecInfo myImageCodecInfo;
                System.Drawing.Imaging.Encoder myEncoder;
                System.Drawing.Imaging.EncoderParameter myEncoderParameter;
                System.Drawing.Imaging.EncoderParameters myEncoderParameters;

                myImageCodecInfo = GetEncoderInfo("image/jpeg");
                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, JpgLevel);
                myEncoderParameters.Param[0] = myEncoderParameter;

                System.Drawing.Bitmap myBitmap;
                myBitmap = new System.Drawing.Bitmap(myImage);
                myImage.Dispose();
                myBitmap.Save(HttpContext.Current.Server.MapPath(ImgFileName), myImageCodecInfo, myEncoderParameters);
                myBitmap.Dispose();
            }
        }
    }
}
