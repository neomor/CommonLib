﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonLib
{
    /// <summary>
    /// 缓存帮助类
    /// </summary>
    public class CacheHelper
    {
        private static Dictionary<string, object> dic = new Dictionary<string, object>();
        // 定义一个静态变量来保存类的实例
        private static CacheHelper session;

        // 定义一个标识确保线程同步
        private static readonly object locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns>返回类型为Session</returns>
        public CacheHelper Instance
        {
            get
            {
                if (session == null)
                {
                    lock (locker)
                    {
                        if (session == null)// 如果类的实例不存在则创建，否则直接返回
                        {
                            session = new CacheHelper();
                        }
                    }
                }
                return session;
            }
        }

        /// <summary>
        /// 删除成员
        /// </summary>
        /// <param name="name"></param>
        public void Remove(string name)
        {
            dic.Remove(name);
        }

        /// <summary>
        /// 删除全部成员
        /// </summary>
        public void RemoveAll()
        {
            dic.Clear();
        }

        /// <summary>
        /// 本类的索引器
        /// </summary>
        /// <returns>返回Object成员</returns>
        public Object this[string index]
        {
            get
            {
                if (dic.ContainsKey(index))
                {
                    Object obj = (Object)dic[index];
                    return obj;
                }
                return null;
            }
            set
            {
                dic.Add(index, value);
            }
        }
    }

}
