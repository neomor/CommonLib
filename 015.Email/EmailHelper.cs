﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// 电子邮件帮助类
    /// </summary>
    public class EmailHelper
    {
        #region 公共属性

        /// <summary>
        /// 邮箱用户名
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// 邮箱密码
        /// </summary>
        [DataMember]
        public string PpassWord { get; set; }

        /// <summary>
        /// Gmail端口号
        /// </summary>
        [DataMember]
        public int Port { get; set; }

        /// <summary>
        /// Gmail 服务器
        /// </summary>
        [DataMember]
        public string Host { get; set; }
        #endregion

        #region 发送邮件

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="collectEmail">收件人 aa@qq.com</param>
        /// <param name="title">标题</param>
        /// <param name="content">内容</param>
        /// <returns></returns>
        public bool SendEmail(string collectEmail, string title, string content)
        {
            var result = false;

            MailMessage msg = new MailMessage();
            msg.To.Add(collectEmail);//收件人
            //发件人信息
            msg.From = new MailAddress(this.UserName, "来自" + this.UserName + "的邮件", System.Text.Encoding.UTF8);
            msg.Subject = title;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = content;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            //msg.IsBodyHtml = true;
            //msg.Priority = MailPriority.High;//优先级

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(this.UserName, this.PpassWord);
            client.Port = this.Port;
            client.Host = this.Host;
            client.EnableSsl = true;
            object userState = msg;
            try
            {
                client.Send(msg);
                result = true;
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #endregion        
    }
}
