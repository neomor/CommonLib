﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    /// <summary>
    /// 实体类帮助类
    /// </summary>
    public class EntityHelper
    {
        #region 设置赋值

        /// <summary>
        /// 设置赋值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="source">源数据对象</param>
        /// <param name="target">赋值目标对象</param>
        public static void SetBase<T, K>(T source, K target) where T : class, new()
        {
            if (source != null && target != null)
            {
                PropertyInfo[] propertys1 = source.GetType().GetProperties();// 获得此模型的公共属性
                PropertyInfo[] propertys = target.GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.CanWrite && propertys1.Where(ex => ex.Name == pi.Name).Count() > 0)
                    {
                        object value = GetValue(source, pi.Name);
                        pi.SetValue(target, value, null);
                    }
                }
            }
        }

        /// <summary>
        /// 设置赋值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="source">源数据对象</param>
        /// <param name="target">赋值目标对象</param>
        public static void SetBase<T, K>(List<T> source, List<K> target) where T : class, new() where K : class, new()
        {
            if (source != null && target != null)
            {
                foreach (var item in source)
                {
                    var obj = new K();
                    SetBase(item, obj);
                    target.Add(obj);
                }
            }
        }

        /// <summary>
        /// 更新赋值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="source">源数据对象</param>
        /// <param name="target">赋值目标对象</param>
        public static void SetUpdateBase<T, K>(T source, K target) where T : class, new()
        {
            PropertyInfo[] propertys1 = source.GetType().GetProperties();// 获得此模型的公共属性
            PropertyInfo[] propertys = target.GetType().GetProperties();
            foreach (PropertyInfo pi in propertys)
            {
                if (pi.CanWrite && propertys1.Where(ex => ex.Name == pi.Name).Count() > 0)
                {
                    object value = GetValue(source, pi.Name);
                    if (value != null)
                    {
                        pi.SetValue(target, value, null);
                    }
                }
            }
        }

        /// <summary>
        /// 获取属性值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static object GetValue<T>(T source, string Name) where T : class, new()
        {
            PropertyInfo[] propertys = source.GetType().GetProperties();// 获得此模型的公共属性
            PropertyInfo pi = propertys.Where(ex => ex.Name == Name).FirstOrDefault();
            if (pi != null)
            {
                return pi.GetValue(source, null);
            }
            return null;
        }
        #endregion
    }
}
