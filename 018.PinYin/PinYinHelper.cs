﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using Microsoft.International.Converters.PinYinConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CommonLib
{
    /// <summary>
    /// 拼音 帮助类
    /// </summary>
    public class PinYinHelper
    {
        #region 获取汉字对应的拼音

        /// <summary>
        /// 获取汉字对应的拼音
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Model.PinYin GetPinYin(string str)
        {
            var chs = str.ToCharArray();
            List<List<string>> Dic = new List<List<string>>();//记录每个汉字的全拼

            foreach (var ch in chs)
            {
                var list = GetPinYin(ch);
                list = list.Where(p => !string.IsNullOrWhiteSpace(p)).Distinct().ToList();//去重
                if (list.Any())
                {
                    Dic.Add(list);
                }
            }

            var obj = new Model.PinYin();
            foreach (var items in Dic)
            {
                if (obj.PingYin.Count <= 0)
                {
                    obj.PingYin = items;
                    obj.FirstPingYin = items.ConvertAll(p => p.Substring(0, 1)).Distinct().ToList();
                }
                else
                {
                    //全拼循环匹配
                    var newTotalPingYins = new List<string>();
                    foreach (var totalPingYin in obj.PingYin)
                    {
                        newTotalPingYins.AddRange(items.Select(item => totalPingYin + item));
                    }
                    newTotalPingYins = newTotalPingYins.Distinct().ToList();
                    obj.PingYin = newTotalPingYins;

                    //首字母循环匹配
                    var newFirstPingYins = new List<string>();
                    foreach (var firstPingYin in obj.FirstPingYin)
                    {
                        newFirstPingYins.AddRange(items.Select(item => firstPingYin + item.Substring(0, 1)));
                    }
                    newFirstPingYins = newFirstPingYins.Distinct().ToList();
                    obj.FirstPingYin = newFirstPingYins;
                }
            }
            return obj;
        } 
        #endregion

        #region 获取单个汉字全拼 包含多音字

        /// <summary>
        /// 获取单个汉字全拼 包含多音字
        /// </summary>
        /// <param name="ch"></param>
        /// <returns></returns>
        private static List<string> GetPinYin(char ch)
        {
            if (ChineseChar.IsValidChar(ch))
            {
                ChineseChar cc = new ChineseChar(ch);
                var list = cc.Pinyins.Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                list = list.ConvertAll(ex => Regex.Replace(ex, @"\d", "").ToLower());//去除声调，转小写  
                return list;
            }
            return new List<string> { ch.ToString() };
        }
        #endregion
        
        #region 汉字转拼音

        #region 汉字 拼音 声调 数组 开始
        static string[] item_hz = { "啊,A,1","啊,A,2","啊,A,3","啊,A,4","啊,A,5","阿,A,1","阿,A,5","阿,E,1","埃,Ai,1","挨,Ai,1","挨,Ai,2","哎,Ai,1","哎,Ai,3","哎,Ai,4","唉,Ai,1","唉,Ai,4"
,"哀,Ai,1","皑,Ai,2","癌,Ai,2","蔼,Ai,3","矮,Ai,3","艾,Ai,4","艾,Yi,4","碍,Ai,4","爱,Ai,4","隘,Ai,4","鞍,An,1","氨,An,1","安,An,1","俺,An,3","按,An,4"
,"暗,An,4","岸,An,4","胺,An,4","案,An,4","肮,Ang,1","昂,Ang,2","盎,Ang,4","凹,Ao,1","凹,Wa,1","敖,Ao,2","熬,Ao,1","熬,Ao,2","翱,Ao,2","袄,Ao,3","傲,Ao,4"
,"奥,Ao,4","懊,Ao,4","澳,Ao,4","芭,Ba,1","捌,Ba,1","扒,Ba,1","扒,Pa,1","扒,Pa,2","叭,Ba,1","吧,Ba,1","吧,Ba,5","笆,Ba,1","八,Ba,1","八,Ba,2","疤,Ba,1"
,"巴,Ba,1","拔,Ba,2","跋,Ba,2","靶,Ba,3","把,Ba,3","把,Ba,4","耙,Ba,4","耙,Pa,2","坝,Ba,4","霸,Ba,4","罢,Ba,4","罢,Ba,5","爸,Ba,4","白,Bai,2","柏,Bai,3"
,"柏,Bo,2","柏,Bo,4","百,Bai,3","百,Bo,2","摆,Bai,3","佰,Bai,3","败,Bai,4","拜,Bai,4","稗,Bai,4","斑,Ban,1","班,Ban,1","搬,Ban,1","扳,Ban,1","扳,Pan,1","般,Ban,1"
,"般,Bo,1","般,Pan,2","颁,Ban,1","板,Ban,3","版,Ban,3","扮,Ban,4","拌,Ban,4","伴,Ban,4","瓣,Ban,4","半,Ban,4","办,Ban,4","绊,Ban,4","邦,Bang,1","帮,Bang,1","梆,Bang,1"
,"榜,Bang,3","膀,Bang,3","膀,Bang,4","膀,Pang,1","膀,Pang,2","绑,Bang,3","棒,Bang,4","磅,Bang,4","磅,Pang,2","蚌,Bang,4","蚌,Beng,4","镑,Bang,4","傍,Bang,4","谤,Bang,4","苞,Bao,1"
,"胞,Bao,1","包,Bao,1","褒,Bao,1","剥,Bao,1","剥,Bo,1","薄,Bao,2","薄,Bo,2","薄,Bo,4","雹,Bao,2","保,Bao,3","堡,Bao,3","堡,Bu,3","堡,Pu,3","堡,Pu,4","饱,Bao,3"
,"宝,Bao,3","抱,Bao,4","报,Bao,4","暴,Bao,4","暴,Pu,4","豹,Bao,4","鲍,Bao,4","爆,Bao,4","杯,Bei,1","碑,Bei,1","悲,Bei,1","卑,Bei,1","北,Bei,3","辈,Bei,4","背,Bei,1"
,"背,Bei,4","贝,Bei,4","钡,Bei,4","倍,Bei,4","狈,Bei,4","备,Bei,4","惫,Bei,4","焙,Bei,4","被,Bei,4","奔,Ben,1","奔,Ben,4","苯,Ben,3","本,Ben,3","笨,Ben,4","崩,Beng,1"
,"绷,Beng,1","绷,Beng,3","绷,Beng,4","甭,Beng,2","泵,Beng,4","蹦,Beng,4","迸,Beng,4","逼,Bi,1","鼻,Bi,2","比,Bi,3","鄙,Bi,3","笔,Bi,3","彼,Bi,3","碧,Bi,4","蓖,Bi,4"
,"蔽,Bi,4","毕,Bi,4","毙,Bi,4","毖,Bi,4","币,Bi,4","庇,Bi,4","痹,Bi,4","闭,Bi,4","敝,Bi,4","弊,Bi,4","必,Bi,4","辟,Bi,4","辟,Pi,1","辟,Pi,4","壁,Bi,4"
,"臂,Bei,5","臂,Bi,4","避,Bi,4","陛,Bi,4","鞭,Bian,1","边,Bian,1","边,Bian,5","编,Bian,1","贬,Bian,3","扁,Bian,3","扁,Pian,1","便,Bian,4","便,Pian,2","变,Bian,4","卞,Bian,4"
,"辨,Bian,4","辩,Bian,4","辫,Bian,4","遍,Bian,4","标,Biao,1","彪,Biao,1","膘,Biao,1","表,Biao,3","鳖,Bie,1","憋,Bie,1","别,Bie,2","别,Bie,4","瘪,Bie,1","瘪,Bie,3","彬,Bin,1"
,"斌,Bin,1","濒,Bin,1","滨,Bin,1","宾,Bin,1","摈,Bin,4","兵,Bing,1","冰,Bing,1","柄,Bing,3","丙,Bing,3","秉,Bing,3","饼,Bing,3","炳,Bing,3","病,Bing,4","并,Bing,1","并,Bing,4"
,"玻,Bo,1","菠,Bo,1","播,Bo,1","拨,Bo,1","钵,Bo,1","波,Bo,1","博,Bo,2","勃,Bo,2","搏,Bo,2","铂,Bo,2","箔,Bo,2","伯,Bai,3","伯,Bo,2","帛,Bo,2","舶,Bo,2"
,"脖,Bo,2","膊,Bo,2","渤,Bo,2","泊,Bo,2","泊,Po,1","驳,Bo,2","捕,Bu,3","卜,Bo,5","卜,Bu,3","哺,Bu,3","补,Bu,3","埠,Bu,4","不,Bu,2","不,Bu,4","布,Bu,4"
,"步,Bu,4","簿,Bu,4","部,Bu,4","怖,Bu,4","擦,Ca,1","猜,Cai,1","裁,Cai,2","材,Cai,2","才,Cai,2","财,Cai,2","睬,Cai,3","踩,Cai,3","采,Cai,3","采,Cai,4","彩,Cai,3"
,"菜,Cai,4","蔡,Cai,4","餐,Can,1","参,Can,1","参,Cen,1","参,Shen,1","蚕,Can,2","残,Can,2","惭,Can,2","惨,Can,3","惨,Can,4","灿,Can,4","苍,Cang,1","舱,Cang,1","仓,Cang,1"
,"沧,Cang,1","藏,Cang,2","藏,Zang,4","操,Cao,1","糙,Cao,1","槽,Cao,2","曹,Cao,2","草,Cao,3","厕,Ce,4","厕,Si,5","策,Ce,4","侧,Ce,4","侧,Ze,4","侧,Zhai,1","册,Ce,4"
,"测,Ce,4","层,Ceng,2","蹭,Ceng,4","插,Cha,1","叉,Cha,1","叉,Cha,2","叉,Cha,3","叉,Cha,4","茬,Cha,2","茶,Cha,2","查,Cha,2","查,Zha,1","碴,Cha,1","碴,Cha,2","搽,Cha,2"
,"察,Cha,2","岔,Cha,4","差,Cha,1","差,Cha,4","差,Chai,1","差,Ci,1","诧,Cha,4","拆,Ca,1","拆,Chai,1","柴,Chai,2","豺,Chai,2","搀,Chan,1","掺,Can,4","掺,Chan,1","掺,Shan,3"
,"蝉,Chan,2","馋,Chan,2","谗,Chan,2","缠,Chan,2","铲,Chan,3","产,Chan,3","阐,Chan,3","颤,Chan,4","颤,Zhan,4","昌,Chang,1","猖,Chang,1","场,Chang,2","场,Chang,3","尝,Chang,2","常,Chang,2"
,"长,Chang,2","长,Zhang,3","偿,Chang,2","肠,Chang,2","厂,An,1","厂,Chang,3","敞,Chang,3","畅,Chang,4","唱,Chang,4","倡,Chang,4","超,Chao,1","抄,Chao,1","钞,Chao,1","朝,Chao,2","朝,Zhao,1"
,"嘲,Chao,2","嘲,Zhao,1","潮,Chao,2","巢,Chao,2","吵,Chao,1","吵,Chao,3","炒,Chao,3","车,Che,1","车,Ju,1","扯,Che,3","撤,Che,4","掣,Che,4","彻,Che,4","澈,Che,4","郴,Chen,1"
,"臣,Chen,2","辰,Chen,2","尘,Chen,2","晨,Chen,2","忱,Chen,2","沉,Chen,2","陈,Chen,2","趁,Chen,4","衬,Chen,4","撑,Cheng,1","称,Chen,4","称,Cheng,1","称,Cheng,4","城,Cheng,2","橙,Chen,2"
,"橙,Cheng,2","成,Cheng,2","呈,Cheng,2","乘,Cheng,2","乘,Sheng,4","程,Cheng,2","惩,Cheng,2","澄,Cheng,2","澄,Deng,4","诚,Cheng,2","承,Cheng,2","逞,Cheng,3","骋,Cheng,3","秤,Chen,4","秤,Cheng,1"
,"秤,Cheng,4","吃,Chi,1","痴,Chi,1","持,Chi,2","匙,Chi,2","匙,Shi,5","池,Chi,2","迟,Chi,2","弛,Chi,2","驰,Chi,2","耻,Chi,3","齿,Chi,3","侈,Chi,3","尺,Che,3","尺,Chi,3"
,"赤,Chi,4","翅,Chi,4","斥,Chi,4","炽,Chi,4","充,Chong,1","冲,Chong,1","冲,Chong,4","虫,Chong,2","崇,Chong,2","宠,Chong,3","抽,Chou,1","酬,Chou,2","畴,Chou,2","踌,Chou,2","稠,Chou,2"
,"愁,Chou,2","筹,Chou,2","仇,Chou,2","仇,Qiu,2","绸,Chou,2","瞅,Chou,3","丑,Chou,3","臭,Chou,4","臭,Xiu,4","初,Chu,1","出,Chu,1","橱,Chu,2","厨,Chu,2","躇,Chu,2","锄,Chu,2"
,"雏,Chu,2","滁,Chu,2","除,Chu,2","楚,Chu,3","础,Chu,3","储,Chu,3","矗,Chu,4","搐,Chu,4","触,Chu,4","处,Chu,3","处,Chu,4","揣,Chuai,1","揣,Chuai,3","揣,Chuai,4","川,Chuan,1"
,"穿,Chuan,1","椽,Chuan,2","传,Chuan,2","传,Zhuan,4","船,Chuan,2","喘,Chuan,3","串,Chuan,4","疮,Chuang,1","窗,Chuang,1","幢,Chuang,2","幢,Zhuang,4","床,Chuang,2","闯,Chuang,3","创,Chuang,1","创,Chuang,4"
,"吹,Chui,1","炊,Chui,1","捶,Chui,2","锤,Chui,2","垂,Chui,2","春,Chun,1","椿,Chun,1","醇,Chun,2","唇,Chun,2","淳,Chun,2","纯,Chun,2","蠢,Chun,3","戳,Chuo,1","绰,Chao,1","绰,Chuo,4"
,"疵,Ci,1","茨,Ci,2","磁,Ci,2","雌,Ci,2","辞,Ci,2","慈,Ci,2","瓷,Ci,2","词,Ci,2","此,Ci,3","刺,Ci,1","刺,Ci,4","赐,Ci,4","次,Ci,4","聪,Cong,1","葱,Cong,1"
,"囱,Cong,1","匆,Cong,1","从,Cong,1","从,Cong,2","丛,Cong,2","凑,Cou,4","粗,Cu,1","醋,Cu,4","簇,Cu,4","促,Cu,4","蹿,Cuan,1","篡,Cuan,4","窜,Cuan,4","摧,Cui,1","崔,Cui,1"
,"催,Cui,1","脆,Cui,4","瘁,Cui,4","粹,Cui,4","淬,Cui,4","翠,Cui,4","村,Cun,1","存,Cun,2","寸,Cun,4","磋,Cuo,1","撮,Cuo,1","撮,Cuo,3","撮,Zuo,3","搓,Cuo,1","措,Cuo,4"
,"挫,Cuo,4","错,Cuo,4","搭,Da,1","达,Da,2","答,Da,1","答,Da,2","瘩,Da,2","打,Da,2","打,Da,3","大,Da,4","大,Dai,4","呆,Ai,2","呆,Dai,1","歹,Dai,3","傣,Dai,3"
,"戴,Dai,4","带,Dai,4","殆,Dai,4","代,Dai,4","贷,Dai,4","袋,Dai,4","待,Dai,1","待,Dai,4","逮,Dai,3","逮,Dai,4","怠,Dai,4","耽,Dan,1","担,Dan,1","担,Dan,3","担,Dan,4"
,"丹,Dan,1","单,Chan,2","单,Dan,1","单,Shan,4","郸,Dan,1","掸,Dan,3","掸,Shan,3","掸,Shan,4","胆,Dan,3","旦,Dan,4","氮,Dan,4","但,Dan,4","惮,Dan,4","淡,Dan,4","诞,Dan,4"
,"弹,Dan,4","弹,Tan,2","蛋,Dan,4","当,Dang,1","当,Dang,4","挡,Dang,3","挡,Dang,4","党,Dang,3","荡,Dang,4","档,Dang,4","刀,Dao,1","捣,Dao,3","蹈,Dao,3","倒,Dao,3","倒,Dao,4"
,"岛,Dao,3","祷,Dao,3","导,Dao,3","到,Dao,4","稻,Dao,4","悼,Dao,4","道,Dao,4","盗,Dao,4","德,De,2","得,De,2","得,De,5","得,Dei,3","的,De,5","的,Di,2","的,Di,4"
,"蹬,Deng,1","蹬,Deng,4","灯,Deng,1","登,Deng,1","等,Deng,3","瞪,Deng,4","凳,Deng,4","邓,Deng,4","堤,Di,1","低,Di,1","滴,Di,1","迪,Di,2","敌,Di,2","笛,Di,2","狄,Di,2"
,"涤,Di,2","翟,Di,2","翟,Zhai,2","嫡,Di,2","抵,Di,3","底,De,5","底,Di,3","地,De,5","地,Di,4","蒂,Di,4","第,Di,4","帝,Di,4","弟,Di,4","递,Di,4","缔,Di,4"
,"颠,Dian,1","掂,Dian,1","滇,Dian,1","碘,Dian,3","点,Dian,3","典,Dian,3","靛,Dian,4","垫,Dian,4","电,Dian,4","佃,Dian,4","佃,Tian,2","甸,Dian,4","店,Dian,4","惦,Dian,4","奠,Dian,4"
,"淀,Dian,4","殿,Dian,4","碉,Diao,1","叼,Diao,1","雕,Diao,1","凋,Diao,1","刁,Diao,1","掉,Diao,4","吊,Diao,4","钓,Diao,4","调,Diao,4","调,Tiao,2","跌,Die,1","爹,Die,1","碟,Die,2"
,"蝶,Die,2","迭,Die,2","谍,Die,2","叠,Die,2","丁,Ding,1","丁,Zheng,1","盯,Ding,1","叮,Ding,1","钉,Ding,1","钉,Ding,4","顶,Ding,3","鼎,Ding,3","锭,Ding,4","定,Ding,4","订,Ding,4"
,"丢,Diu,1","东,Dong,1","冬,Dong,1","董,Dong,3","懂,Dong,3","动,Dong,4","栋,Dong,4","侗,Dong,4","侗,Tong,2","侗,Tong,3","恫,Dong,4","恫,Tong,1","冻,Dong,4","洞,Dong,4","兜,Dou,1"
,"抖,Dou,3","斗,Dou,3","斗,Dou,4","陡,Dou,3","豆,Dou,4","逗,Dou,4","痘,Dou,4","都,Dou,1","都,Du,1","督,Du,1","毒,Du,2","犊,Du,2","独,Du,2","读,Dou,4","读,Du,2"
,"堵,Du,3","睹,Du,3","赌,Du,3","杜,Du,4","镀,Du,4","肚,Du,3","肚,Du,4","度,Du,4","度,Duo,2","渡,Du,4","妒,Du,4","端,Duan,1","短,Duan,3","锻,Duan,4","段,Duan,4"
,"断,Duan,4","缎,Duan,4","堆,Dui,1","堆,Zui,1","兑,Dui,4","队,Dui,4","对,Dui,4","墩,Dun,1","吨,Dun,1","蹲,Cun,2","蹲,Dun,1","敦,Dui,4","敦,Dun,1","顿,Du,2","顿,Dun,4"
,"囤,Dun,4","囤,Tun,2","钝,Dun,4","盾,Dun,4","遁,Dun,4","掇,Duo,1","哆,Duo,1","多,Duo,1","夺,Duo,2","垛,Duo,3","垛,Duo,4","躲,Duo,3","朵,Duo,3","跺,Duo,4","舵,Duo,4"
,"舵,Tuo,2","剁,Duo,4","惰,Duo,4","堕,Duo,4","堕,Hui,1","蛾,E,2","蛾,Yi,3","峨,E,2","鹅,E,2","俄,E,2","额,E,2","讹,E,2","娥,E,2","恶,E,3","恶,E,4"
,"恶,Wu,1","恶,Wu,4","厄,E,4","扼,E,4","遏,E,4","鄂,E,4","饿,E,4","恩,En,1","而,Er,2","儿,Er,2","耳,Er,3","尔,Er,3","饵,Er,3","洱,Er,3","二,Er,4"
,"贰,Er,4","发,Fa,1","发,Fa,4","罚,Fa,2","筏,Fa,2","伐,Fa,2","乏,Fa,2","阀,Fa,2","法,Fa,3","珐,Fa,4","藩,Fan,1","帆,Fan,1","番,Fan,1","番,Pan,1","翻,Fan,1"
,"樊,Fan,2","矾,Fan,2","钒,Fan,2","繁,Fan,2","繁,Po,2","凡,Fan,2","烦,Fan,2","反,Fan,3","返,Fan,3","范,Fan,4","贩,Fan,4","犯,Fan,4","饭,Fan,4","泛,Fan,2","泛,Fan,4"
,"坊,Fang,1","坊,Fang,2","芳,Fang,1","方,Fang,1","肪,Fang,2","房,Fang,2","防,Fang,2","妨,Fang,1","妨,Fang,2","仿,Fang,3","访,Fang,3","纺,Fang,3","放,Fang,4","菲,Fei,1","菲,Fei,3"
,"非,Fei,1","啡,Fei,1","飞,Fei,1","肥,Fei,2","匪,Fei,3","诽,Fei,3","吠,Fei,4","肺,Fei,4","废,Fei,4","沸,Fei,4","费,Fei,4","芬,Fen,1","酚,Fen,1","吩,Fen,1","氛,Fen,1"
,"分,Fen,1","分,Fen,4","纷,Fen,1","坟,Fen,2","焚,Fen,2","汾,Fen,2","粉,Fen,3","奋,Fen,4","份,Fen,4","忿,Fen,4","愤,Fen,4","粪,Fen,4","丰,Feng,1","封,Feng,1","枫,Feng,1"
,"蜂,Feng,1","峰,Feng,1","锋,Feng,1","风,Feng,1","疯,Feng,1","烽,Feng,1","逢,Feng,2","冯,Feng,2","冯,Ping,2","缝,Feng,2","缝,Feng,4","讽,Feng,3","奉,Feng,4","凤,Feng,4","佛,Fo,2"
,"佛,Fu,2","否,Fou,3","否,Pi,3","夫,Fu,1","夫,Fu,2","敷,Fu,1","肤,Fu,1","孵,Fu,1","扶,Fu,2","拂,Bi,4","拂,Fu,2","辐,Fu,2","幅,Fu,2","氟,Fu,2","符,Fu,2"
,"伏,Fu,2","俘,Fu,2","服,Fu,2","服,Fu,4","浮,Fu,2","涪,Fu,2","福,Fu,2","袱,Fu,2","弗,Fu,2","甫,Fu,3","抚,Fu,3","辅,Fu,3","俯,Fu,3","釜,Fu,3","斧,Fu,3"
,"脯,Fu,3","脯,Pu,2","腑,Fu,3","府,Fu,3","腐,Fu,3","赴,Fu,4","副,Fu,4","覆,Fu,4","赋,Fu,4","复,Fu,4","傅,Fu,4","付,Fu,4","阜,Fu,4","父,Fu,3","父,Fu,4"
,"腹,Fu,4","负,Fu,4","富,Fu,4","讣,Fu,4","附,Fu,4","妇,Fu,4","缚,Fu,4","咐,Fu,4","咐,Fu,5","噶,Ga,2","嘎,Ga,1","嘎,Ga,2","嘎,Ga,3","该,Gai,1","改,Gai,3"
,"概,Gai,4","钙,Gai,4","盖,Gai,4","盖,Ge,3","溉,Gai,4","干,Gan,1","干,Gan,4","甘,Gan,1","杆,Gan,1","杆,Gan,3","柑,Gan,1","竿,Gan,1","肝,Gan,1","赶,Gan,3","感,Gan,3"
,"秆,Gan,3","敢,Gan,3","赣,Gan,4","冈,Gang,1","刚,Gang,1","钢,Gang,1","钢,Gang,4","缸,Gang,1","肛,Gang,1","纲,Gang,1","岗,Gang,3","港,Gang,3","杠,Gang,1","杠,Gang,4","篙,Gao,1"
,"皋,Gao,1","高,Gao,1","膏,Gao,1","膏,Gao,4","羔,Gao,1","糕,Gao,1","搞,Gao,3","镐,Gao,3","镐,Hao,4","稿,Gao,3","告,Gao,4","哥,Ge,1","歌,Ge,1","搁,Ge,1","搁,Ge,2"
,"戈,Ge,1","鸽,Ge,1","胳,Ga,1","胳,Ge,1","胳,Ge,2","疙,Ge,1","割,Ge,1","革,Ge,2","革,Ji,2","革,Ji,3","葛,Ge,2","葛,Ge,3","格,Ge,1","格,Ge,2","蛤,Ge,2"
,"蛤,Ha,2","阁,Ge,2","隔,Ge,2","铬,Ge,4","个,Ge,3","个,Ge,4","各,Ge,3","各,Ge,4","给,Gei,3","给,Ji,3","根,Gen,1","跟,Gen,1","耕,Geng,1","更,Geng,1","更,Geng,4"
,"庚,Geng,1","羹,Geng,1","埂,Geng,3","耿,Geng,3","梗,Geng,3","工,Gong,1","攻,Gong,1","功,Gong,1","恭,Gong,1","龚,Gong,1","供,Gong,1","供,Gong,4","躬,Gong,1","公,Gong,1","宫,Gong,1"
,"弓,Gong,1","巩,Gong,3","汞,Gong,3","拱,Gong,3","贡,Gong,4","共,Gong,1","共,Gong,4","钩,Gou,1","勾,Gou,1","勾,Gou,4","沟,Gou,1","苟,Gou,3","狗,Gou,3","垢,Gou,4","构,Gou,4"
,"购,Gou,4","够,Gou,4","辜,Gu,1","菇,Gu,1","咕,Gu,1","箍,Gu,1","估,Gu,1","估,Gu,4","沽,Gu,1","孤,Gu,1","姑,Gu,1","鼓,Gu,3","古,Gu,3","蛊,Gu,3","骨,Gu,1"
,"骨,Gu,2","骨,Gu,3","谷,Gu,3","谷,Yu,4","股,Gu,3","故,Gu,4","顾,Gu,4","固,Gu,4","雇,Gu,4","刮,Gua,1","瓜,Gua,1","剐,Gua,3","寡,Gua,3","挂,Gua,4","褂,Gua,4"
,"乖,Guai,1","拐,Guai,3","怪,Guai,4","棺,Guan,1","关,Guan,1","官,Guan,1","冠,Guan,1","冠,Guan,4","观,Guan,1","观,Guan,4","管,Guan,3","馆,Guan,3","罐,Guan,4","惯,Guan,4","灌,Guan,4"
,"贯,Guan,4","光,Guang,1","广,An,1","广,Guang,3","逛,Guang,4","瑰,Gui,1","规,Gui,1","圭,Gui,1","硅,Gui,1","归,Gui,1","龟,Gui,1","龟,Jun,1","龟,Qiu,1","闺,Gui,1","轨,Gui,3"
,"鬼,Gui,3","诡,Gui,3","癸,Gui,3","桂,Gui,4","柜,Gui,4","柜,Ju,3","跪,Gui,4","贵,Gui,4","刽,Gui,4","辊,Gun,3","滚,Gun,3","棍,Gun,4","锅,Guo,1","郭,Guo,1","国,Guo,2"
,"果,Guo,3","裹,Guo,3","过,Guo,1","过,Guo,4","哈,Ha,1","哈,Ha,3","哈,Ha,4","骸,Hai,2","孩,Hai,2","海,Hai,3","氦,Hai,4","亥,Hai,4","害,Hai,4","骇,Hai,4","酣,Han,1"
,"憨,Han,1","邯,Han,2","韩,Han,2","含,Han,2","涵,Han,2","寒,Han,2","函,Han,2","喊,Han,3","罕,Han,3","翰,Han,4","撼,Han,4","捍,Han,4","旱,Han,4","憾,Han,4","悍,Han,4"
,"焊,Han,4","汗,Han,2","汗,Han,4","汉,Han,4","夯,Ben,4","夯,Hang,1","杭,Hang,2","航,Hang,2","壕,Hao,2","嚎,Hao,2","豪,Hao,2","毫,Hao,2","郝,Hao,3","好,Hao,3","好,Hao,4"
,"耗,Hao,4","号,Hao,2","号,Hao,4","浩,Hao,4","呵,A,1","呵,A,2","呵,A,3","呵,A,4","呵,A,5","呵,He,1","呵,Ke,1","喝,He,1","喝,He,4","荷,He,2","荷,He,4"
,"菏,He,2","核,He,2","核,Hu,2","禾,He,2","和,He,2","和,He,4","和,Hu,2","和,Huo,2","和,Huo,4","何,He,2","何,He,4","合,Ge,3","合,He,2","盒,He,2","貉,Hao,2"
,"貉,He,2","貉,Mo,4","阂,He,2","河,He,2","涸,He,2","赫,He,4","褐,He,4","鹤,He,4","贺,He,4","嘿,Hai,1","嘿,Hei,1","嘿,Mo,4","黑,Hei,1","痕,Hen,2","很,Hen,3"
,"狠,Hen,3","恨,Hen,4","哼,Heng,1","哼,Hng,5","亨,Heng,1","横,Heng,2","横,Heng,4","衡,Heng,2","恒,Heng,2","轰,Hong,1","哄,Hong,1","哄,Hong,3","哄,Hong,4","烘,Hong,1","虹,Hong,2"
,"虹,Jiang,4","鸿,Hong,2","洪,Hong,2","宏,Hong,2","弘,Hong,2","红,Gong,1","红,Hong,2","喉,Hou,2","侯,Hou,2","侯,Hou,4","猴,Hou,2","吼,Hou,3","厚,Hou,4","候,Hou,4","后,Hou,4"
,"呼,Hu,1","乎,Hu,1","忽,Hu,1","瑚,Hu,2","壶,Hu,2","葫,Hu,2","胡,Hu,2","蝴,Hu,2","狐,Hu,2","糊,Hu,1","糊,Hu,2","糊,Hu,4","湖,Hu,2","弧,Hu,2","虎,Hu,3"
,"虎,Hu,4","唬,Hu,3","唬,Xia,4","护,Hu,4","互,Hu,4","沪,Hu,4","户,Hu,4","花,Hua,1","哗,Hua,1","哗,Hua,2","哗,Ye,4","华,Hua,1","华,Hua,2","华,Hua,4","猾,Hua,2"
,"滑,Hua,2","画,Hua,4","划,Hua,2","划,Hua,4","划,Huai,5","化,Hua,1","化,Hua,4","话,Hua,4","槐,Huai,2","徊,Huai,2","徊,Hui,2","怀,Huai,2","淮,Huai,2","坏,Huai,4","坏,Pi,1"
,"欢,Huan,1","环,Huan,2","桓,Huan,2","还,Hai,2","还,Huan,2","缓,Huan,3","换,Huan,4","患,Huan,4","唤,Huan,4","痪,Huan,4","豢,Huan,4","焕,Huan,4","涣,Huan,4","宦,Huan,4","幻,Huan,4"
,"荒,Huang,1","慌,Huang,1","黄,Huang,2","磺,Huang,2","蝗,Huang,2","簧,Huang,2","皇,Huang,2","凰,Huang,2","惶,Huang,2","煌,Huang,2","晃,Huang,3","晃,Huang,4","幌,Huang,3","恍,Huang,3","谎,Huang,3"
,"灰,Hui,1","挥,Hui,1","辉,Hui,1","徽,Hui,1","恢,Hui,1","蛔,Hui,2","回,Hui,2","毁,Hui,3","悔,Hui,3","慧,Hui,4","卉,Hui,4","惠,Hui,4","晦,Hui,4","贿,Hui,4","秽,Hui,4"
,"会,Hui,4","会,Kuai,4","烩,Hui,4","汇,Hui,4","讳,Hui,4","诲,Hui,4","绘,Hui,4","荤,Hun,1","荤,Xun,1","昏,Hun,1","婚,Hun,1","魂,Hun,2","浑,Hun,2","混,Hun,2","混,Hun,4"
,"豁,Hua,2","豁,Huo,1","豁,Huo,4","活,Huo,2","伙,Huo,3","火,Huo,3","获,Huo,4","或,Huo,4","惑,Huo,4","霍,Huo,4","货,Huo,4","祸,Huo,4","击,Ji,1","圾,Ji,1","基,Ji,1"
,"机,Ji,1","畸,Ji,1","稽,Ji,1","稽,Qi,3","积,Ji,1","箕,Ji,1","肌,Ji,1","饥,Ji,1","迹,Ji,1","激,Ji,1","讥,Ji,1","鸡,Ji,1","姬,Ji,1","绩,Ji,1","缉,Ji,1"
,"缉,Qi,1","吉,Ji,2","极,Ji,2","棘,Ji,2","辑,Ji,2","籍,Ji,2","集,Ji,2","及,Ji,2","急,Ji,2","疾,Ji,2","汲,Ji,2","即,Ji,2","嫉,Ji,2","级,Ji,2","挤,Ji,3"
,"几,Ji,1","几,Ji,3","脊,Ji,2","脊,Ji,3","己,Ji,3","蓟,Ji,4","技,Ji,4","冀,Ji,4","季,Ji,4","伎,Ji,4","祭,Ji,4","祭,Zhai,4","剂,Ji,4","悸,Ji,4","济,Ji,3"
,"济,Ji,4","寄,Ji,4","寂,Ji,4","计,Ji,4","记,Ji,4","既,Ji,4","忌,Ji,4","际,Ji,4","妓,Ji,4","继,Ji,4","纪,Ji,3","纪,Ji,4","嘉,Jia,1","枷,Jia,1","夹,Ga,1"
,"夹,Jia,1","夹,Jia,2","佳,Jia,1","家,Jia,1","家,Jia,5","家,Jie,5","加,Jia,1","荚,Jia,2","颊,Jia,2","贾,Gu,3","贾,Jia,3","甲,Jia,3","钾,Jia,3","假,Jia,3","假,Jia,4"
,"稼,Jia,4","价,Jia,4","价,Jie,4","价,Jie,5","架,Jia,4","驾,Jia,4","嫁,Jia,4","歼,Jian,1","监,Jian,1","监,Jian,4","坚,Jian,1","尖,Jian,1","笺,Jian,1","间,Jian,1","间,Jian,4"
,"煎,Jian,1","兼,Jian,1","肩,Jian,1","艰,Jian,1","奸,Jian,1","缄,Jian,1","茧,Jian,3","检,Jian,3","柬,Jian,3","碱,Jian,3","硷,Jian,3","拣,Jian,3","捡,Jian,3","简,Jian,3","俭,Jian,3"
,"剪,Jian,3","减,Jian,3","荐,Jian,4","槛,Jian,4","槛,Kan,3","鉴,Jian,4","践,Jian,4","贱,Jian,4","见,Jian,4","见,Xian,4","键,Jian,4","箭,Jian,4","件,Jian,4","健,Jian,4","舰,Jian,4"
,"剑,Jian,4","饯,Jian,4","渐,Jian,1","渐,Jian,4","溅,Jian,1","溅,Jian,4","涧,Jian,4","建,Jian,4","僵,Jiang,1","姜,Jiang,1","将,Jiang,1","将,Jiang,4","将,Qiang,1","浆,Jiang,1","浆,Jiang,4"
,"江,Jiang,1","疆,Jiang,1","蒋,Jiang,3","桨,Jiang,3","奖,Jiang,3","讲,Jiang,3","匠,Jiang,4","酱,Jiang,4","降,Jiang,4","降,Xiang,2","蕉,Jiao,1","蕉,Qiao,2","椒,Jiao,1","礁,Jiao,1","焦,Jiao,1"
,"胶,Jiao,1","交,Jiao,1","郊,Jiao,1","浇,Jiao,1","骄,Jiao,1","娇,Jiao,1","嚼,Jiao,2","嚼,Jiao,4","嚼,Jue,2","搅,Jia,3","搅,Jiao,3","铰,Jia,3","铰,Jiao,3","矫,Jia,3","矫,Jiao,2"
,"矫,Jiao,3","侥,Jia,3","侥,Jiao,3","侥,Yao,2","脚,Jia,3","脚,Jiao,3","脚,Jue,2","狡,Jia,3","狡,Jiao,3","角,Jia,3","角,Jiao,3","角,Jue,2","饺,Jia,3","饺,Jiao,3","缴,Jia,3"
,"缴,Jiao,3","缴,Zhuo,2","绞,Jia,3","绞,Jiao,3","剿,Chao,1","剿,Jia,3","剿,Jiao,3","教,Jiao,1","教,Jiao,4","酵,Jiao,4","轿,Jiao,4","较,Jiao,4","叫,Jiao,4","窖,Jiao,4","揭,Jie,1"
,"接,Jie,1","皆,Jie,1","秸,Jie,1","街,Jie,1","阶,Jie,1","截,Jie,2","劫,Jie,2","节,Jie,1","节,Jie,2","桔,Jie,2","桔,Ju,2","杰,Jie,2","捷,Jie,2","睫,Jie,2","竭,Jie,2"
,"洁,Jie,2","结,Jie,1","结,Jie,2","解,Jie,3","解,Jie,4","解,Xie,4","姐,Jie,3","戒,Jie,4","藉,Ji,2","藉,Jie,4","芥,Gai,4","芥,Jie,4","界,Jie,4","借,Jie,4","介,Jie,4"
,"疥,Jie,4","诫,Jie,4","届,Jie,4","巾,Jin,1","筋,Jin,1","斤,Jin,1","金,Jin,1","今,Jin,1","津,Jin,1","襟,Jin,1","紧,Jin,3","锦,Jin,3","仅,Jin,3","仅,Jin,4","谨,Jin,3"
,"进,Jin,4","靳,Jin,4","晋,Jin,4","禁,Jin,1","禁,Jin,4","近,Jin,4","烬,Jin,4","浸,Jin,4","尽,Jin,3","尽,Jin,4","劲,Jin,4","劲,Jing,4","荆,Jing,1","兢,Jing,1","茎,Jing,1"
,"睛,Jing,1","晶,Jing,1","鲸,Jing,1","京,Jing,1","惊,Jing,1","精,Jing,1","粳,Jing,1","经,Jing,1","经,Jing,4","井,Jing,3","警,Jing,3","景,Jing,3","颈,Geng,3","颈,Jing,3","静,Jing,4"
,"境,Jing,4","敬,Jing,4","镜,Jing,4","径,Jing,4","痉,Jing,4","靖,Jing,4","竟,Jing,4","竞,Jing,4","净,Jing,4","炯,Jiong,3","窘,Jiong,3","揪,Jiu,1","究,Jiu,1","纠,Jiu,1","玖,Jiu,3"
,"韭,Jiu,3","久,Jiu,3","灸,Jiu,3","九,Jiu,3","酒,Jiu,3","厩,Jiu,4","救,Jiu,4","旧,Jiu,4","臼,Jiu,4","舅,Jiu,4","咎,Jiu,4","就,Jiu,4","疚,Jiu,4","鞠,Ju,1","拘,Ju,1"
,"狙,Ju,1","疽,Ju,1","居,Ju,1","驹,Ju,1","菊,Ju,2","局,Ju,2","咀,Ju,3","咀,Zui,3","矩,Ju,3","举,Ju,3","沮,Ju,3","沮,Ju,4","聚,Ju,4","拒,Ju,4","据,Ju,1"
,"据,Ju,4","巨,Ju,4","具,Ju,4","距,Ju,4","踞,Ju,4","锯,Ju,1","锯,Ju,4","俱,Ju,1","俱,Ju,4","句,Gou,1","句,Ju,4","惧,Ju,4","炬,Ju,4","剧,Ju,4","捐,Juan,1"
,"鹃,Juan,1","娟,Juan,1","倦,Juan,4","眷,Juan,4","卷,Juan,3","卷,Juan,4","绢,Juan,4","撅,Jue,1","攫,Jue,2","抉,Jue,2","掘,Jue,2","倔,Jue,2","倔,Jue,4","爵,Jue,2","觉,Jiao,4"
,"觉,Jue,2","决,Jue,2","诀,Jue,2","绝,Jue,2","均,Jun,1","均,Yun,4","菌,Jun,1","菌,Jun,4","钧,Jun,1","军,Jun,1","君,Jun,1","峻,Jun,4","俊,Juan,4","俊,Jun,4","竣,Jun,4"
,"浚,Jun,4","浚,Xun,4","郡,Jun,4","骏,Jun,4","喀,Ka,1","咖,Ga,1","咖,Ka,1","卡,Ka,3","卡,Qia,3","咯,Ge,1","咯,Ka,3","咯,Lo,5","咯,Luo,4","开,Kai,1","揩,Kai,1"
,"楷,Jie,1","楷,Kai,3","凯,Kai,3","慨,Kai,3","刊,Kan,1","堪,Kan,1","勘,Kan,1","坎,Kan,3","砍,Kan,3","看,Kan,1","看,Kan,4","康,Kang,1","慷,Kang,1","糠,Kang,1","扛,Gang,1"
,"扛,Kang,2","抗,Kang,4","亢,Kang,4","炕,Kang,4","考,Kao,3","拷,Kao,3","烤,Kao,3","靠,Kao,4","坷,Ke,1","坷,Ke,3","苛,Ke,1","柯,Ke,1","棵,Ke,1","磕,Ke,1","颗,Ke,1"
,"科,Ke,1","壳,Ke,2","壳,Qiao,4","咳,Hai,1","咳,Ke,2","可,Ke,3","可,Ke,4","渴,Ke,3","克,Ke,4","刻,Ke,4","客,Ke,4","课,Ke,4","肯,Ken,3","啃,Ken,3","垦,Ken,3"
,"恳,Ken,3","坑,Keng,1","吭,Hang,2","吭,Keng,1","空,Kong,1","空,Kong,4","恐,Kong,3","孔,Kong,3","控,Kong,4","抠,Kou,1","口,Kou,3","扣,Kou,4","寇,Kou,4","枯,Ku,1","哭,Ku,1"
,"窟,Ku,1","苦,Ku,3","酷,Ku,4","库,Ku,4","裤,Ku,4","夸,Kua,1","垮,Kua,3","挎,Kua,4","跨,Kua,4","胯,Kua,4","块,Kuai,4","筷,Kuai,4","侩,Kuai,4","快,Kuai,4","宽,Kuan,1"
,"款,Kuan,3","匡,Kuang,1","筐,Kuang,1","狂,Kuang,2","框,Kuang,1","框,Kuang,4","矿,Kuang,4","眶,Kuang,4","旷,Kuang,4","况,Kuang,4","亏,Kui,1","盔,Kui,1","岿,Kui,1","窥,Kui,1","葵,Kui,2"
,"奎,Kui,2","魁,Kui,2","傀,Gui,1","傀,Kui,3","馈,Kui,4","愧,Kui,4","溃,Hui,4","溃,Kui,4","坤,Kun,1","昆,Kun,1","捆,Kun,3","困,Kun,4","括,Gua,1","括,Kuo,4","扩,Kuo,4"
,"廓,Kuo,4","阔,Kuo,4","垃,La,1","拉,La,1","拉,La,2","拉,La,3","拉,La,4","喇,La,1","喇,La,2","喇,La,3","蜡,La,4","蜡,Zha,4","腊,La,4","腊,Xi,1","辣,La,4"
,"啦,La,1","啦,La,5","莱,Lai,2","来,Lai,2","赖,Lai,4","蓝,La,5","蓝,Lan,2","婪,Lan,2","栏,Lan,2","拦,Lan,2","篮,Lan,2","阑,Lan,2","兰,Lan,2","澜,Lan,2","谰,Lan,2"
,"揽,Lan,3","览,Lan,3","懒,Lan,3","缆,Lan,3","烂,Lan,4","滥,Lan,4","琅,Lang,2","榔,Lang,2","狼,Lang,2","廊,Lang,2","郎,Lang,2","郎,Lang,4","朗,Lang,3","浪,Lang,4","捞,Lao,1"
,"劳,Lao,2","牢,Lao,2","老,Lao,3","佬,Lao,3","姥,Lao,3","姥,Mu,3","酪,Lao,4","烙,Lao,4","烙,Luo,4","涝,Lao,4","勒,Le,4","勒,Lei,1","乐,Le,4","乐,Yue,4","雷,Lei,2"
,"镭,Lei,2","蕾,Lei,3","磊,Lei,3","累,Lei,2","累,Lei,3","累,Lei,4","儡,Lei,3","垒,Lei,3","擂,Lei,1","擂,Lei,2","擂,Lei,4","肋,Le,1","肋,Lei,4","类,Lei,4","泪,Lei,4"
,"棱,Leng,1","棱,Leng,2","棱,Ling,2","楞,Leng,2","冷,Leng,3","厘,Li,2","梨,Li,2","犁,Li,2","黎,Li,2","篱,Li,2","狸,Li,2","离,Li,2","漓,Li,2","理,Li,3","李,Li,3"
,"里,Li,3","鲤,Li,3","礼,Li,3","莉,Li,4","荔,Li,4","吏,Li,4","栗,Li,4","丽,Li,2","丽,Li,4","厉,Li,4","励,Li,4","砾,Li,4","历,Li,4","利,Li,4","傈,Li,4"
,"例,Li,4","俐,Li,4","痢,Li,4","立,Li,4","粒,Li,4","沥,Li,4","隶,Li,4","力,Li,4","璃,Li,2","璃,Li,5","哩,Li,1","哩,Li,3","哩,Li,5","俩,Lia,3","俩,Liang,3"
,"联,Lian,2","莲,Lian,2","连,Lian,2","镰,Lian,2","廉,Lian,2","怜,Lian,2","涟,Lian,2","帘,Lian,2","敛,Lian,3","脸,Lian,3","链,Lian,4","恋,Lian,4","炼,Lian,4","练,Lian,4","粮,Liang,2"
,"凉,Liang,2","凉,Liang,4","梁,Liang,2","粱,Liang,2","良,Liang,2","两,Liang,3","辆,Liang,4","量,Liang,2","量,Liang,4","晾,Liang,4","亮,Liang,4","谅,Liang,4","撩,Liao,1","撩,Liao,2","撩,Liao,4"
,"聊,Liao,2","僚,Liao,2","疗,Liao,2","燎,Liao,2","燎,Liao,3","寥,Liao,2","辽,Liao,2","潦,Lao,3","潦,Liao,3","了,Le,5","了,Liao,3","了,Liao,4","撂,Liao,1","撂,Liao,2","撂,Liao,4"
,"镣,Liao,4","廖,Liao,4","料,Liao,4","列,Lie,4","裂,Lie,3","裂,Lie,4","烈,Lie,4","劣,Lie,4","猎,Lie,4","琳,Lin,2","林,Lin,2","磷,Lin,2","霖,Lin,2","临,Lin,2","邻,Lin,2"
,"鳞,Lin,2","淋,Lin,2","淋,Lin,4","凛,Lin,3","赁,Lin,4","吝,Lin,4","拎,Ling,1","玲,Ling,2","菱,Ling,2","零,Ling,2","龄,Ling,2","铃,Ling,2","伶,Ling,2","羚,Ling,2","凌,Ling,2"
,"灵,Ling,2","陵,Ling,2","岭,Ling,3","领,Ling,3","另,Ling,4","令,Ling,2","令,Ling,3","令,Ling,4","溜,Liu,1","溜,Liu,4","琉,Liu,2","榴,Liu,2","硫,Liu,2","馏,Liu,2","馏,Liu,4"
,"留,Liu,2","刘,Liu,2","瘤,Liu,2","流,Liu,2","柳,Liu,3","六,Liu,4","六,Lu,4","龙,Long,2","聋,Long,2","咙,Long,2","笼,Long,2","笼,Long,3","窿,Long,2","隆,Long,1","隆,Long,2"
,"垄,Long,3","拢,Long,3","陇,Long,3","楼,Lou,2","娄,Lou,2","搂,Lou,1","搂,Lou,3","篓,Lou,3","漏,Lou,4","陋,Lou,4","芦,Lu,2","芦,Lu,3","卢,Lu,2","颅,Lu,2","庐,Lu,2"
,"炉,Lu,2","掳,Lu,3","卤,Lu,3","虏,Lu,3","鲁,Lu,3","麓,Lu,4","碌,Liu,4","碌,Lu,4","露,Lou,4","露,Lu,4","路,Lu,4","赂,Lu,4","鹿,Lu,4","潞,Lu,4","禄,Lu,4"
,"录,Lu,4","陆,Liu,4","陆,Lu,4","戮,Lu,4","驴,Lu:,5","吕,Lu:,3","铝,Lu:,3","侣,Lu:,3","旅,Lu:,3","履,Lu:,3","屡,Lu:,3","缕,Lu:,3","虑,Lu:,4","氯,Lu:,4","律,Lu:,4"
,"率,Lu:,4","率,Shuai,4","滤,Lu:,4","绿,Lu,4","绿,Lu:,4","峦,Luan,2","挛,Luan,2","孪,Luan,2","滦,Luan,2","卵,Luan,3","乱,Luan,4","掠,Lue:,3","掠,Lue:,4","略,Lue:,4","抡,Lun,1"
,"抡,Lun,2","轮,Lun,2","伦,Lun,2","仑,Lun,2","沦,Lun,2","纶,Guan,1","纶,Lun,2","论,Lun,2","论,Lun,4","萝,Luo,2","螺,Luo,2","罗,Luo,1","罗,Luo,2","罗,Luo,5","逻,Luo,2"
,"锣,Luo,2","箩,Luo,2","骡,Luo,2","裸,Luo,3","落,La,4","落,Lao,4","落,Luo,1","落,Luo,4","洛,Luo,4","骆,Luo,4","络,Lao,4","络,Luo,4","妈,Ma,1","麻,Ma,1","麻,Ma,2"
,"玛,Ma,3","码,Ma,3","蚂,Ma,1","蚂,Ma,3","蚂,Ma,4","马,Ma,3","骂,Ma,4","嘛,Ma,5","吗,Ma,2","吗,Ma,3","吗,Ma,5","埋,Mai,2","埋,Man,2","买,Mai,3","麦,Mai,4"
,"卖,Mai,4","迈,Mai,4","脉,Mai,4","脉,Mo,4","瞒,Man,2","馒,Man,2","蛮,Man,2","满,Man,3","蔓,Man,2","蔓,Man,4","蔓,Wan,4","曼,Man,4","慢,Man,4","漫,Man,4","谩,Man,2"
,"谩,Man,4","芒,Mang,2","芒,Wang,2","茫,Mang,2","盲,Mang,2","氓,Mang,2","氓,Meng,2","忙,Mang,2","莽,Mang,3","猫,Mao,1","猫,Mao,2","茅,Mao,2","锚,Mao,2","毛,Mao,2","矛,Mao,2"
,"铆,Mao,3","卯,Mao,3","茂,Mao,4","冒,Mao,4","冒,Mo,4","帽,Mao,4","貌,Mao,4","贸,Mao,4","么,Ma,5","么,Me,5","玫,Mei,2","枚,Mei,2","梅,Mei,2","酶,Mei,2","霉,Mei,2"
,"煤,Mei,2","没,Mei,2","没,Mo,4","眉,Mei,2","媒,Mei,2","镁,Mei,3","每,Mei,3","美,Mei,3","昧,Mei,4","寐,Mei,4","妹,Mei,4","媚,Mei,4","门,Men,2","闷,Men,1","闷,Men,4"
,"们,Men,5","萌,Meng,2","蒙,Meng,1","蒙,Meng,2","蒙,Meng,3","檬,Meng,2","盟,Meng,2","盟,Ming,2","锰,Meng,3","猛,Meng,3","梦,Meng,4","孟,Meng,4","眯,Mi,1","眯,Mi,3","醚,Mi,2"
,"靡,Mi,2","靡,Mi,3","糜,Mei,2","糜,Mi,2","迷,Mi,2","谜,Mei,4","谜,Mi,2","弥,Mi,2","米,Mi,3","秘,Bi,4","秘,Lin,2","秘,Mi,4","觅,Mi,4","泌,Bi,4","泌,Mi,4"
,"蜜,Mi,4","密,Mi,4","幂,Mi,4","棉,Mian,2","眠,Mian,2","绵,Mian,2","冕,Mian,3","免,Mian,3","勉,Mian,3","娩,Mian,3","娩,Wan,3","缅,Mian,3","面,Mian,4","苗,Miao,2","描,Miao,2"
,"瞄,Miao,2","藐,Miao,3","秒,Miao,3","渺,Miao,3","庙,Miao,4","妙,Miao,4","蔑,Mie,4","灭,Mie,4","民,Min,2","抿,Min,3","皿,Min,3","敏,Min,3","悯,Min,3","闽,Min,3","明,Ming,2"
,"螟,Ming,2","鸣,Ming,2","铭,Ming,2","名,Ming,2","命,Ming,4","谬,Miu,4","摸,Mo,1","摸,Mo,2","摹,Mo,2","蘑,Mo,2","模,Mo,2","模,Mu,2","膜,Mo,2","磨,Mo,2","磨,Mo,4"
,"摩,Ma,1","摩,Mo,2","魔,Mo,2","抹,Ma,1","抹,Mo,3","抹,Mo,4","末,Mo,4","莫,Mo,4","墨,Mo,4","默,Mo,4","沫,Mo,4","漠,Mo,4","寞,Mo,4","陌,Mo,4","谋,Mou,2"
,"牟,Mou,2","牟,Mu,4","某,Mou,3","拇,Mu,3","牡,Mu,3","亩,Mu,3","姆,Mu,3","母,Mu,3","墓,Mu,4","暮,Mu,4","幕,Mu,4","募,Mu,4","慕,Mu,4","木,Mu,4","目,Mu,4"
,"睦,Mu,4","牧,Mu,4","穆,Mu,4","拿,Na,2","哪,Na,3","哪,Na,5","哪,Nai,3","哪,Ne,2","哪,Nei,3","呐,Na,4","呐,Na,5","呐,Ne,4","呐,Ne,5","钠,Na,4","那,Na,1"
,"那,Na,3","那,Na,4","那,Nei,4","娜,Na,4","娜,Nuo,2","纳,Na,4","氖,Nai,3","乃,Nai,3","奶,Nai,3","耐,Nai,4","奈,Nai,4","南,Na,1","南,Nan,2","男,Nan,2","难,Nan,2"
,"难,Nan,4","囊,Nang,1","囊,Nang,2","挠,Nao,2","脑,Nao,3","恼,Nao,3","闹,Nao,4","淖,Nao,4","呢,Na,4","呢,Ne,4","呢,Ne,5","呢,Ni,2","馁,Nei,3","内,Nei,4","嫩,Nen,4"
,"能,Neng,2","妮,Ni,1","霓,Ni,2","倪,Ni,2","泥,Ni,2","泥,Ni,4","尼,Ni,2","拟,Ni,3","你,Ni,3","匿,Ni,4","腻,Ni,4","逆,Ni,4","溺,Ni,4","溺,Niao,4","蔫,Nian,1"
,"拈,Nian,1","年,Nian,2","碾,Nian,3","撵,Nian,3","捻,Nian,3","念,Nian,4","娘,Niang,2","酿,Nian,4","酿,Niang,2","酿,Niang,4","鸟,Diao,3","鸟,Niao,3","尿,Ni,4","尿,Niao,4","尿,Sui,1"
,"捏,Nie,1","聂,Nie,4","孽,Nie,4","啮,Nie,4","镊,Nie,4","镍,Nie,4","涅,Nie,4","您,Nin,2","柠,Ning,2","狞,Ning,2","凝,Ning,2","宁,Ning,2","宁,Ning,4","拧,Ning,2","拧,Ning,3"
,"拧,Ning,4","泞,Ning,4","牛,Niu,2","扭,Niu,3","钮,Niu,3","纽,Niu,3","脓,Nong,2","浓,Nong,2","农,Nong,2","弄,Long,4","弄,Nong,4","奴,Nu,2","努,Nao,2","努,Nu,3","怒,Nu,4"
,"女,Nu:,3","暖,Nuan,3","虐,Nue:,4","疟,Nue:,4","疟,Yao,4","挪,Nuo,2","懦,Nuo,4","糯,Nuo,4","诺,Nuo,4","哦,E,2","哦,O,2","哦,O,4","哦,Wo,2","哦,Wo,4","欧,Ou,1"
,"鸥,Ou,1","殴,Ou,1","藕,Ou,3","呕,Ou,3","呕,Ou,4","偶,Ou,3","沤,Ou,1","沤,Ou,4","啪,Pa,1","趴,Pa,1","爬,Pa,2","帕,Pa,4","怕,Pa,4","琶,Pa,5","拍,Pai,1"
,"排,Pai,2","排,Pai,3","牌,Pai,2","徘,Pai,2","湃,Pai,4","派,Pa,1","派,Pai,4","攀,Pan,1","潘,Pan,1","盘,Pan,2","磐,Pan,2","盼,Pan,4","畔,Pan,4","判,Pan,4","叛,Pan,4"
,"乓,Pang,1","庞,Pang,2","旁,Pang,2","耪,Pang,3","胖,Pan,2","胖,Pang,4","抛,Pao,1","咆,Pao,2","刨,Bao,4","刨,Pao,2","炮,Bao,1","炮,Pao,1","炮,Pao,2","炮,Pao,4","袍,Pao,2"
,"跑,Pao,2","跑,Pao,3","泡,Pao,1","泡,Pao,4","呸,Pei,1","胚,Pei,1","培,Pei,2","裴,Pei,2","赔,Pei,2","陪,Pei,2","配,Pei,4","佩,Pei,4","沛,Pei,4","喷,Pen,1","喷,Pen,4"
,"盆,Pen,2","砰,Peng,1","抨,Peng,1","烹,Peng,1","澎,Peng,1","澎,Peng,2","彭,Peng,2","蓬,Peng,2","棚,Peng,2","硼,Peng,2","篷,Peng,2","膨,Peng,2","朋,Peng,2","鹏,Peng,2","捧,Peng,3"
,"碰,Peng,4","坯,Pi,1","砒,Pi,1","霹,Pi,1","批,Pi,1","披,Pi,1","劈,Pi,1","劈,Pi,3","琵,Pi,2","毗,Pi,2","啤,Pi,2","脾,Pi,2","疲,Pi,2","皮,Pi,2","匹,Pi,3"
,"匹,Ya,3","痞,Pi,3","僻,Pi,4","屁,Pi,4","譬,Pi,4","篇,Pian,1","偏,Pian,1","片,Pian,1","片,Pian,4","骗,Pian,4","飘,Piao,1","漂,Piao,1","漂,Piao,3","漂,Piao,4","瓢,Piao,2"
,"票,Piao,4","撇,Pie,1","撇,Pie,3","瞥,Pie,1","拼,Pin,1","频,Pin,2","贫,Pin,2","品,Pin,3","聘,Pin,4","乒,Ping,1","坪,Ping,2","苹,Pin,2","苹,Ping,2","萍,Ping,2","平,Ping,2"
,"凭,Ping,2","瓶,Ping,2","评,Ping,2","屏,Bing,3","屏,Ping,2","坡,Po,1","泼,Po,1","颇,Po,1","婆,Po,2","破,Po,4","魄,Bo,2","魄,Po,4","魄,Tuo,4","迫,Pai,3","迫,Po,4"
,"粕,Po,4","剖,Pou,1","扑,Pu,1","铺,Pu,1","铺,Pu,4","仆,Pu,1","仆,Pu,2","莆,Pu,2","葡,Pu,2","菩,Pu,2","蒲,Pu,2","埔,Bu,4","埔,Pu,3","朴,Piao,2","朴,Po,1"
,"朴,Po,4","朴,Pu,3","圃,Pu,3","普,Pu,3","浦,Pu,3","谱,Pu,3","曝,Bao,4","曝,Pu,4","瀑,Bao,4","瀑,Pu,4","期,Ji,1","期,Qi,1","欺,Qi,1","栖,Qi,1","栖,Xi,1"
,"戚,Qi,1","妻,Qi,1","妻,Qi,4","七,Qi,1","凄,Qi,1","漆,Qi,1","柒,Qi,1","沏,Qi,1","其,Ji,1","其,Qi,2","棋,Qi,2","奇,Ji,1","奇,Qi,2","歧,Qi,2","畦,Qi,2"
,"畦,Qi,2","崎,Qi,2","脐,Qi,2","齐,Ji,4","齐,Qi,2","齐,Qi,4","旗,Qi,2","祈,Qi,2","祁,Qi,2","骑,Qi,2","起,Qi,3","岂,Qi,3","乞,Qi,3","企,Qi,3","启,Qi,3"
,"契,Qi,4","契,Xie,4","砌,Qi,4","砌,Qie,4","器,Qi,4","气,Qi,4","迄,Qi,4","弃,Qi,4","汽,Qi,4","泣,Qi,4","讫,Qi,4","掐,Qia,1","恰,Qia,4","洽,Qia,4","牵,Qian,1"
,"扦,Qian,1","钎,Qian,1","铅,Qian,1","铅,Yan,2","千,Qian,1","迁,Qian,1","签,Qian,1","仟,Qian,1","谦,Qian,1","乾,Gan,1","乾,Qian,2","黔,Qian,2","钱,Qian,2","钳,Qian,2","前,Qian,2"
,"潜,Qian,2","遣,Qian,3","浅,Jian,1","浅,Qian,3","谴,Qian,3","堑,Qian,4","嵌,Kan,4","嵌,Qian,4","欠,Qian,4","歉,Qian,4","枪,Qiang,1","呛,Qiang,1","呛,Qiang,4","腔,Qiang,1","羌,Qiang,1"
,"墙,Qiang,2","蔷,Qiang,2","强,Jiang,4","强,Qiang,2","强,Qiang,3","抢,Qiang,1","抢,Qiang,3","橇,Qiao,1","锹,Qiao,1","敲,Qiao,1","悄,Qiao,1","悄,Qiao,3","桥,Qiao,2","瞧,Qiao,2","瞧,Ya,3"
,"乔,Qiao,2","侨,Qiao,2","巧,Qiao,3","鞘,Qiao,4","鞘,Shao,1","撬,Qiao,4","翘,Qiao,2","翘,Qiao,4","峭,Qiao,4","俏,Qiao,4","窍,Qiao,4","切,Qie,1","切,Qie,4","茄,Jia,1","茄,Qie,2"
,"且,Ju,1","且,Qie,3","怯,Qie,4","窃,Qie,4","钦,Qin,1","侵,Qin,1","亲,Qin,1","亲,Qing,4","秦,Qin,2","琴,Qin,2","勤,Qin,2","芹,Qin,2","擒,Qin,2","禽,Qin,2","寝,Qin,3"
,"沁,Qin,4","青,Qing,1","轻,Qing,1","氢,Qing,1","倾,Qing,1","卿,Qing,1","清,Qing,1","擎,Qing,2","晴,Qing,2","氰,Qing,2","情,Qing,2","顷,Qing,3","请,Qing,3","庆,Qing,4","琼,Qiong,2"
,"穷,Qiong,2","秋,Qiu,1","丘,Qiu,1","邱,Qiu,1","球,Qiu,2","求,Qiu,2","囚,Qiu,2","酋,Qiu,2","泅,Qiu,2","趋,Qu,1","区,Ou,1","区,Qu,1","蛆,Qu,1","曲,Qu,1","曲,Qu,3"
,"躯,Qu,1","屈,Qu,1","驱,Qu,1","渠,Qu,2","取,Qu,3","娶,Qu,3","龋,Qu,3","趣,Qu,4","去,Qu,4","圈,Juan,1","圈,Juan,4","圈,Quan,1","颧,Quan,2","权,Quan,2","醛,Quan,2"
,"泉,Quan,2","全,Quan,2","痊,Quan,2","拳,Quan,2","犬,Quan,3","券,Quan,4","券,Xuan,4","劝,Quan,4","缺,Que,1","炔,Gui,4","炔,Que,1","瘸,Que,2","却,Que,4","鹊,Que,4","榷,Que,4"
,"确,Que,4","雀,Qiao,1","雀,Qiao,3","雀,Que,4","裙,Qun,2","群,Qun,2","然,Ran,2","燃,Ran,2","冉,Ran,3","染,Ran,3","瓤,Rang,2","壤,Rang,3","攘,Rang,3","嚷,Rang,1","嚷,Rang,3"
,"让,Rang,4","饶,Rao,2","扰,Rao,3","绕,Rao,3","绕,Rao,4","惹,Re,3","热,Re,4","壬,Ren,2","仁,Ren,2","人,Ren,2","忍,Ren,3","韧,Ren,4","任,Ren,2","任,Ren,4","认,Ren,4"
,"刃,Ren,4","妊,Ren,4","纫,Ren,4","扔,Reng,1","仍,Reng,2","日,Ri,4","戎,Rong,2","茸,Rong,2","蓉,Rong,2","荣,Rong,2","融,Rong,2","熔,Rong,2","溶,Rong,2","容,Rong,2","绒,Rong,2"
,"冗,Rong,3","揉,Rou,2","柔,Rou,2","肉,Rou,4","茹,Ru,2","蠕,Ru,2","儒,Ru,2","孺,Ru,2","如,Ru,2","辱,Ru,3","乳,Ru,3","汝,Ru,3","入,Ru,4","褥,Ru,4","软,Ruan,3"
,"阮,Ruan,3","蕊,Rui,3","瑞,Rui,4","锐,Rui,4","闰,Run,4","润,Run,4","若,Re,3","若,Ruo,4","弱,Ruo,4","撒,Sa,1","撒,Sa,3","洒,Sa,3","萨,Sa,4","腮,Sai,1","鳃,Sai,1"
,"塞,Sai,1","塞,Sai,4","塞,Se,4","赛,Sai,4","三,San,1","叁,San,1","伞,San,3","散,San,3","散,San,4","桑,Sang,1","嗓,Sang,3","丧,Sang,1","丧,Sang,4","搔,Sao,1","骚,Sao,1"
,"扫,Sao,3","扫,Sao,4","嫂,Sao,3","瑟,Se,4","色,Se,4","色,Shai,3","涩,Se,4","森,Sen,1","僧,Seng,1","莎,Sha,1","莎,Suo,1","砂,Sha,1","杀,Sha,1","刹,Cha,4","刹,Sha,1"
,"沙,Sha,1","沙,Sha,4","纱,Sha,1","傻,Sha,3","啥,Sha,4","煞,Sha,1","煞,Sha,4","筛,Shai,1","晒,Shai,4","珊,Shan,1","苫,Shan,1","苫,Shan,4","杉,Sha,1","杉,Shan,1","山,Shan,1"
,"删,Shan,1","煽,Shan,1","衫,Shan,1","闪,Shan,3","陕,Shan,3","擅,Shan,4","赡,Shan,4","膳,Shan,4","善,Shan,4","汕,Shan,4","扇,Shan,1","扇,Shan,4","缮,Shan,4","墒,Shang,1","伤,Shang,1"
,"商,Shang,1","赏,Shang,3","晌,Shang,3","上,Shang,3","上,Shang,4","尚,Shang,4","裳,Chang,2","裳,Shang,5","梢,Sao,4","梢,Shao,1","捎,Shao,1","捎,Shao,4","稍,Shao,1","稍,Shao,4","烧,Shao,1"
,"芍,Shao,2","勺,Biao,1","勺,Shao,2","韶,Shao,2","少,Shao,3","少,Shao,4","哨,Shao,4","邵,Shao,4","绍,Shao,4","奢,She,1","赊,She,1","蛇,She,2","蛇,Yi,2","舌,She,2","舍,She,3"
,"舍,She,4","赦,She,4","摄,She,4","射,She,4","慑,She,4","涉,She,4","社,She,4","设,She,4","砷,Shen,1","申,Shen,1","呻,Shen,1","伸,Shen,1","身,Shen,1","深,Shen,1","娠,Shen,1"
,"绅,Shen,1","神,Shen,2","沈,Chen,2","沈,Shen,3","审,Shen,3","婶,Shen,3","甚,Shen,2","甚,Shen,4","肾,Shen,4","慎,Shen,4","渗,Shen,4","声,Sheng,1","生,Sheng,1","甥,Sheng,1","牲,Sheng,1"
,"升,Sheng,1","绳,Sheng,2","省,Sheng,3","省,Xing,3","盛,Cheng,2","盛,Sheng,4","剩,Sheng,4","胜,Sheng,1","胜,Sheng,4","圣,Sheng,4","师,Shi,1","失,Shi,1","狮,Shi,1","施,Shi,1","湿,Shi,1"
,"诗,Shi,1","尸,Shi,1","虱,Shi,1","十,Shi,2","石,Dan,4","石,Shi,2","拾,Shi,2","时,Shi,2","什,Shen,2","什,Shi,2","食,Shi,2","食,Si,4","蚀,Shi,2","实,Shi,2","识,Shi,2"
,"识,Zhi,4","史,Shi,3","矢,Shi,3","使,Shi,3","屎,Shi,3","驶,Shi,3","始,Shi,3","式,Shi,4","示,Shi,4","士,Shi,4","世,Shi,4","柿,Shi,4","事,Shi,4","拭,Shi,4","誓,Shi,4"
,"逝,Shi,4","势,Shi,4","是,Shi,4","嗜,Shi,4","噬,Shi,4","适,Kuo,4","适,Shi,4","仕,Shi,4","侍,Shi,4","释,Shi,4","饰,Shi,4","氏,Shi,4","氏,Zhi,1","市,Shi,4","恃,Shi,4"
,"室,Shi,4","视,Shi,4","试,Shi,4","收,Shou,1","手,Shou,3","首,Shou,3","守,Shou,3","寿,Shou,4","授,Shou,4","售,Shou,4","受,Shou,4","瘦,Shou,4","兽,Shou,4","蔬,Shu,1","枢,Shu,1"
,"梳,Shu,1","殊,Shu,1","抒,Shu,1","输,Shu,1","叔,Shu,1","舒,Shu,1","淑,Shu,1","疏,Shu,1","书,Shu,1","赎,Shu,2","孰,Shu,2","熟,Shou,2","熟,Shu,2","薯,Shu,3","暑,Shu,3"
,"曙,Shu,3","署,Shu,3","蜀,Shu,3","黍,Shu,3","鼠,Shu,3","属,Shu,3","属,Zhu,3","术,Shu,4","术,Zhu,2","述,Shu,4","树,Shu,4","束,Shu,4","戍,Shu,4","竖,Shu,4","墅,Shu,4"
,"庶,Shu,4","数,Shu,3","数,Shu,4","数,Shuo,4","漱,Shu,4","恕,Shu,4","刷,Shua,1","刷,Shua,4","耍,Shua,3","摔,Shuai,1","衰,Cui,1","衰,Shuai,1","甩,Shuai,3","帅,Shuai,4","栓,Shuan,1"
,"拴,Shuan,1","霜,Shuang,1","双,Shuang,1","爽,Shuang,3","谁,Shei,2","谁,Shui,2","水,Shui,3","睡,Shui,4","税,Shui,4","吮,Shun,3","瞬,Shun,4","顺,Shun,4","舜,Shun,4","说,Shui,4","说,Shuo,1"
,"说,Yue,4","硕,Shuo,4","朔,Shuo,4","烁,Shuo,4","斯,Si,1","撕,Si,1","嘶,Si,1","思,Sai,1","思,Si,1","私,Si,1","司,Si,1","丝,Si,1","死,Si,3","肆,Si,4","寺,Si,4"
,"嗣,Si,4","四,Si,4","伺,Ci,4","伺,Si,4","似,Shi,4","似,Si,4","饲,Si,4","巳,Si,4","松,Song,1","耸,Song,3","怂,Song,3","颂,Song,4","送,Song,4","宋,Song,4","讼,Song,4"
,"诵,Song,4","搜,Sou,1","艘,Sou,1","擞,Sou,3","擞,Sou,4","嗽,Sou,4","苏,Su,1","酥,Su,1","俗,Su,2","素,Su,4","速,Su,4","粟,Su,4","僳,Su,4","塑,Su,4","溯,Su,4"
,"宿,Su,4","宿,Xiu,3","宿,Xiu,4","诉,Su,4","肃,Su,4","酸,Suan,1","蒜,Suan,4","算,Suan,4","虽,Sui,1","隋,Sui,2","随,Sui,2","绥,Sui,2","髓,Sui,3","碎,Sui,4","岁,Sui,4"
,"穗,Sui,4","遂,Sui,2","遂,Sui,4","隧,Sui,4","祟,Sui,4","孙,Sun,1","损,Sun,3","笋,Sun,3","蓑,Suo,1","梭,Suo,1","唆,Suo,1","缩,Su,4","缩,Suo,1","琐,Suo,3","索,Suo,3"
,"锁,Suo,3","所,Suo,3","塌,Ta,1","他,Ta,1","它,Ta,1","她,Ta,1","塔,Da,5","塔,Ta,3","獭,Ta,3","挞,Ta,4","蹋,Ta,4","踏,Ta,1","踏,Ta,4","胎,Tai,1","苔,Tai,1"
,"苔,Tai,2","抬,Tai,2","台,Tai,1","台,Tai,2","泰,Tai,4","酞,Tai,4","太,Tai,4","态,Tai,4","汰,Tai,4","坍,Tan,1","摊,Tan,1","贪,Tan,1","瘫,Tan,1","滩,Tan,1","坛,Tan,2"
,"檀,Tan,2","痰,Tan,2","潭,Tan,2","谭,Tan,2","谈,Tan,2","坦,Tan,3","毯,Tan,3","袒,Tan,3","碳,Tan,4","探,Tan,4","叹,Tan,4","炭,Tan,4","汤,Shang,1","汤,Tang,1","塘,Tang,2"
,"搪,Tang,2","堂,Tang,2","棠,Tang,2","膛,Tang,2","唐,Tang,2","糖,Tang,2","倘,Chang,2","倘,Tang,3","躺,Tang,3","淌,Tang,3","趟,Tang,1","趟,Tang,4","烫,Tang,4","掏,Tao,1","涛,Tao,1"
,"滔,Tao,1","绦,Tao,1","萄,Tao,2","桃,Tao,2","逃,Tao,2","淘,Tao,2","陶,Tao,2","陶,Yao,2","讨,Tao,3","套,Tao,4","特,Te,4","藤,Teng,2","腾,Teng,2","疼,Teng,2","誊,Teng,2"
,"梯,Ti,1","剔,Ti,1","踢,Ti,1","锑,Ti,1","提,Di,1","提,Ti,2","题,Ti,2","蹄,Ti,2","啼,Ti,2","体,Ti,1","体,Ti,3","替,Ti,4","嚏,Ti,4","惕,Ti,4","涕,Ti,4"
,"剃,Ti,4","屉,Ti,4","天,Tian,1","添,Tian,1","填,Tian,2","田,Tian,2","甜,Tian,2","恬,Tian,2","舔,Tian,3","腆,Tian,3","挑,Tiao,1","挑,Tiao,3","条,Tiao,2","迢,Tiao,2","眺,Tiao,4"
,"跳,Tiao,4","贴,Tie,1","铁,Tie,3","帖,Tie,1","帖,Tie,3","帖,Tie,4","厅,Ting,1","听,Ting,1","烃,Ting,1","汀,Ting,1","廷,Ting,2","停,Ting,2","亭,Ting,2","庭,Ting,2","挺,Ting,3"
,"艇,Ting,3","通,Tong,1","通,Tong,4","桐,Tong,2","酮,Tong,2","瞳,Tong,2","同,Tong,2","同,Tong,4","铜,Tong,2","彤,Tong,2","童,Tong,2","桶,Tong,3","捅,Tong,3","筒,Tong,3","统,Tong,3"
,"痛,Tong,4","偷,Tou,1","投,Tou,2","头,Tou,2","透,Tou,4","凸,Tu,1","秃,Tu,1","突,Tu,1","图,Tu,2","徒,Tu,2","途,Tu,2","涂,Tu,2","屠,Tu,2","土,Tu,3","吐,Tu,3"
,"吐,Tu,4","兔,Tu,4","湍,Tuan,1","团,Tuan,2","推,Tui,1","颓,Tui,2","腿,Tui,3","蜕,Tui,4","褪,Tui,4","褪,Tun,4","退,Tui,4","吞,Tun,1","屯,Tun,2","屯,Zhun,1","臀,Tun,2"
,"拖,Tuo,1","托,Tuo,1","脱,Tuo,1","鸵,Tuo,2","陀,Tuo,2","驮,Duo,4","驮,Tuo,2","驼,Tuo,2","椭,Tuo,3","妥,Tuo,3","拓,Ta,4","拓,Tuo,4","唾,Tuo,4","挖,Wa,1","哇,Wa,1"
,"哇,Wa,5","蛙,Wa,1","洼,Wa,1","娃,Wa,2","瓦,Wa,3","瓦,Wa,4","袜,Wa,4","歪,Wai,1","外,Wai,4","豌,Wan,1","弯,Wan,1","湾,Wan,1","玩,Wan,2","顽,Wan,2","丸,Wan,2"
,"烷,Wan,2","完,Wan,2","碗,Wan,3","挽,Wan,3","晚,Wan,3","皖,Wan,3","惋,Wan,3","宛,Wan,3","婉,Wan,3","万,Mo,4","万,Wan,4","腕,Wan,4","汪,Wang,1","王,Wang,2","王,Wang,4"
,"亡,Wang,2","亡,Wu,2","枉,Wang,3","网,Wang,3","往,Wang,3","往,Wang,4","旺,Wang,4","望,Wang,4","忘,Wang,2","忘,Wang,4","妄,Wang,4","威,Wei,1","巍,Wei,1","微,Wei,1","危,Wei,1"
,"韦,Wei,2","违,Wei,2","桅,Wei,2","围,Wei,2","唯,Wei,2","唯,Wei,3","惟,Wei,2","为,Wei,2","为,Wei,4","潍,Wei,2","维,Wei,2","苇,Wei,3","萎,Wei,1","萎,Wei,3","委,Wei,1"
,"委,Wei,3","伟,Wei,3","伪,Wei,3","尾,Wei,3","尾,Yi,3","纬,Wei,3","未,Wei,4","蔚,Wei,4","蔚,Yu,4","味,Wei,4","畏,Wei,4","胃,Wei,4","喂,Wei,4","魏,Wei,4","位,Wei,4"
,"渭,Wei,4","谓,Wei,4","尉,Wei,4","尉,Yu,4","慰,Wei,4","卫,Wei,4","瘟,Wen,1","温,Wen,1","蚊,Wen,2","文,Wen,2","闻,Wen,2","纹,Wen,2","纹,Wen,4","吻,Wen,3","稳,Wen,3"
,"紊,Wen,3","问,Wen,4","嗡,Weng,1","翁,Weng,1","瓮,Weng,4","挝,Wo,1","挝,Zhua,1","蜗,Wo,1","涡,Guo,1","涡,Wo,1","窝,Wo,1","我,Wo,3","斡,Wo,4","卧,Wo,4","握,Wo,4"
,"沃,Wo,4","巫,Wu,1","呜,Wu,1","钨,Wu,1","乌,Wu,1","乌,Wu,4","污,Wu,1","诬,Wu,1","屋,Wu,1","无,Mo,2","无,Wu,2","芜,Wu,2","梧,Wu,2","吾,Wu,2","吴,Wu,2"
,"毋,Wu,2","武,Wu,3","五,Wu,3","捂,Wu,2","捂,Wu,3","午,Wu,3","舞,Wu,3","伍,Wu,3","侮,Wu,3","坞,Wu,4","戊,Wu,4","雾,Wu,4","晤,Wu,4","物,Wu,4","勿,Wu,4"
,"务,Wu,4","悟,Wu,4","误,Wu,4","昔,Xi,1","熙,Xi,1","析,Xi,1","西,Xi,1","硒,Xi,1","矽,Xi,1","晰,Xi,1","嘻,Xi,1","吸,Xi,1","锡,Xi,1","牺,Xi,1","稀,Xi,1"
,"息,Xi,1","希,Xi,1","悉,Xi,1","膝,Xi,1","夕,Xi,1","惜,Xi,1","熄,Xi,1","烯,Xi,1","溪,Xi,1","汐,Xi,1","犀,Xi,1","檄,Xi,2","袭,Xi,2","席,Xi,2","习,Xi,2"
,"媳,Xi,2","喜,Xi,3","铣,Xi,3","铣,Xian,3","洗,Xi,3","洗,Xian,3","系,Ji,4","系,Xi,4","隙,Xi,4","戏,Hu,1","戏,Xi,4","细,Xi,4","瞎,Xia,1","虾,Ha,2","虾,Xia,1"
,"匣,Xia,2","霞,Xia,2","辖,Xia,2","暇,Xia,2","峡,Xia,2","侠,Xia,2","狭,Xia,2","下,Xia,4","厦,Sha,4","厦,Xia,4","夏,Xia,4","吓,He,4","吓,Xia,4","掀,Xian,1","锨,Xian,1"
,"先,Xian,1","仙,Xian,1","鲜,Xian,1","鲜,Xian,3","纤,Qian,4","纤,Xian,1","咸,Xian,2","贤,Xian,2","衔,Xian,2","舷,Xian,2","闲,Xian,2","涎,Xian,2","弦,Xian,2","嫌,Xian,2","显,Xian,3"
,"险,Xian,3","现,Xian,4","献,Xian,4","县,Xian,4","腺,Xian,4","馅,Xian,4","羡,Xian,4","宪,Xian,4","陷,Xian,4","限,Xian,4","线,Xian,4","相,Xiang,1","相,Xiang,4","厢,Xiang,1","镶,Xiang,1"
,"香,Xiang,1","箱,Xiang,1","襄,Xiang,1","湘,Xiang,1","乡,Xiang,1","翔,Xiang,2","祥,Xiang,2","详,Xiang,2","想,Xiang,3","响,Xiang,3","享,Xiang,3","项,Xiang,4","巷,Hang,4","巷,Xiang,4","橡,Xiang,4"
,"像,Xiang,4","向,Xiang,4","象,Xiang,4","萧,Xiao,1","硝,Xiao,1","霄,Xiao,1","削,Xiao,1","削,Xue,1","哮,Xiao,1","嚣,Ao,2","嚣,Xiao,1","销,Xiao,1","消,Xiao,1","宵,Xiao,1","淆,Xiao,2"
,"晓,Xiao,3","小,Xiao,3","孝,Xiao,4","校,Jiao,4","校,Xiao,4","肖,Xiao,1","肖,Xiao,4","啸,Xiao,4","笑,Xiao,4","效,Xiao,4","楔,Xie,1","些,Xie,1","歇,Xie,1","蝎,Xie,1","鞋,Xie,2"
,"协,Xie,2","挟,Jia,1","挟,Xie,2","携,Xie,2","邪,Xie,2","邪,Ye,2","斜,Xie,2","胁,Xie,2","谐,Xie,2","写,Xie,3","写,Xie,4","械,Xie,4","卸,Xie,4","蟹,Xie,4","懈,Xie,4"
,"泄,Xie,4","泻,Xie,4","谢,Xie,4","屑,Xie,4","薪,Xin,1","芯,Xin,1","芯,Xin,4","锌,Xin,1","欣,Xin,1","辛,Xin,1","新,Xin,1","忻,Xin,1","心,Xin,1","信,Xin,4","衅,Xin,4"
,"星,Xing,1","腥,Xing,1","猩,Xing,1","惺,Xing,1","兴,Xing,1","兴,Xing,4","刑,Xing,2","型,Xing,2","形,Xing,2","邢,Xing,2","行,Hang,2","行,Hang,4","行,Heng,2","行,Xing,2","醒,Xing,3"
,"幸,Xing,4","杏,Xing,4","性,Xing,4","姓,Xing,4","兄,Xiong,1","凶,Xiong,1","胸,Xiong,1","匈,Xiong,1","汹,Xiong,1","雄,Xiong,2","熊,Xiong,2","休,Xiu,1","修,Xiu,1","羞,Xiu,1","朽,Xiu,3"
,"嗅,Xiu,4","锈,Xiu,4","秀,Xiu,4","袖,Xiu,4","绣,Xiu,4","墟,Xu,1","戌,Qu,5","戌,Xu,1","需,Xu,1","虚,Xu,1","嘘,Shi,1","嘘,Xu,1","须,Xu,1","徐,Xu,2","许,Xu,3"
,"蓄,Xu,4","酗,Xu,4","叙,Xu,4","旭,Xu,4","序,Xu,4","畜,Chu,4","畜,Xu,4","恤,Xu,4","絮,Xu,4","婿,Xu,4","绪,Xu,4","续,Xu,4","轩,Xuan,1","喧,Xuan,1","宣,Xuan,1"
,"悬,Xuan,2","旋,Xuan,2","旋,Xuan,4","玄,Xuan,2","选,Xuan,3","癣,Xuan,3","眩,Xuan,4","绚,Xuan,4","靴,Xue,1","薛,Xue,1","学,Xue,2","穴,Xue,2","雪,Xue,3","血,Xie,3","血,Xue,4"
,"勋,Xun,1","熏,Xun,1","熏,Xun,4","循,Xun,2","旬,Xun,2","询,Xun,2","寻,Xin,2","寻,Xun,2","驯,Xun,2","巡,Xun,2","殉,Xun,4","汛,Xun,4","训,Xun,4","讯,Xun,4","逊,Xun,4"
,"迅,Xun,4","压,Ya,1","压,Ya,4","押,Ya,1","鸦,Ya,1","鸭,Ya,1","呀,Ya,1","呀,Ya,5","丫,Ya,1","芽,Ya,2","牙,Ya,2","蚜,Ya,2","崖,Ya,2","衙,Ya,2","涯,Ya,2"
,"雅,Ya,1","雅,Ya,3","哑,Ya,1","亚,Ya,4","讶,Ya,4","焉,Yan,1","咽,Yan,1","咽,Yan,4","咽,Ye,4","阉,Yan,1","烟,Yan,1","烟,Yin,1","淹,Yan,1","盐,Yan,2","严,Yan,2"
,"研,Yan,2","研,Yan,4","蜒,Yan,2","岩,Yan,2","延,Yan,2","言,Yan,2","颜,Yan,2","阎,Yan,2","炎,Yan,2","沿,Yan,2","沿,Yan,4","奄,Yan,3","掩,Yan,3","眼,Yan,3","衍,Yan,3"
,"演,Yan,3","艳,Yan,4","堰,Yan,4","燕,Yan,1","燕,Yan,4","厌,Yan,4","砚,Yan,4","雁,Yan,4","唁,Yan,4","彦,Yan,4","焰,Yan,4","宴,Yan,4","谚,Yan,4","验,Yan,4","殃,Yang,1"
,"央,Yang,1","鸯,Yang,1","秧,Yang,1","杨,Yang,2","扬,Yang,2","佯,Yang,2","疡,Yang,2","羊,Yang,2","洋,Yang,2","阳,Yang,2","氧,Yang,3","仰,Yang,3","痒,Yang,3","养,Yang,3","样,Yang,4"
,"漾,Yang,4","邀,Yao,1","腰,Yao,1","妖,Yao,1","瑶,Yao,2","摇,Yao,2","尧,Yao,2","遥,Yao,2","窑,Yao,2","谣,Yao,2","姚,Yao,2","咬,Yao,3","舀,Yao,3","药,Yao,4","要,Yao,1"
,"要,Yao,4","耀,Yao,4","椰,Ye,1","噎,Ye,1","耶,Ye,1","耶,Ye,2","爷,Ye,2","野,Ye,3","冶,Ye,3","也,Ye,3","页,Ye,4","掖,Ye,1","掖,Ye,4","业,Ye,4","叶,Xie,2"
,"叶,Ye,4","曳,Ye,4","曳,Zhuai,4","腋,Ye,4","夜,Ye,4","液,Ye,4","一,Yi,1","壹,Yi,1","医,Yi,1","揖,Yi,1","铱,Yi,1","依,Yi,1","伊,Yi,1","衣,Yi,1","衣,Yi,3"
,"衣,Yi,4","颐,Yi,2","夷,Yi,2","遗,Wei,4","遗,Yi,2","移,Yi,2","仪,Yi,2","胰,Yi,2","疑,Yi,2","沂,Yi,2","宜,Yi,2","姨,Yi,2","彝,Yi,2","椅,Yi,1","椅,Yi,3"
,"蚁,Yi,3","倚,Yi,3","已,Yi,3","乙,Yi,3","矣,Yi,3","以,Yi,3","艺,Yi,4","抑,Yi,4","易,Yi,4","邑,Yi,4","屹,Ge,1","屹,Yi,4","亿,Yi,4","役,Yi,4","臆,Yi,4"
,"逸,Yi,4","肄,Yi,4","疫,Yi,4","亦,Yi,4","裔,Yi,4","意,Yi,4","毅,Yi,4","忆,Yi,4","义,Yi,4","益,Yi,4","溢,Yi,4","诣,Yi,4","议,Yi,4","谊,Yi,4","译,Yi,4"
,"异,Yi,4","翼,Yi,4","翌,Yi,4","绎,Yi,4","茵,Yin,1","荫,Yin,1","荫,Yin,4","因,Yin,1","殷,Yan,1","殷,Yin,1","殷,Yin,3","音,Yin,1","阴,Yin,1","姻,Yin,1","吟,Yin,2"
,"银,Yin,2","淫,Yin,2","寅,Yin,2","饮,Yin,3","饮,Yin,4","尹,Yin,3","引,Yin,3","隐,Yin,3","印,Yin,4","英,Ying,1","樱,Ying,1","婴,Ying,1","鹰,Ying,1","应,Ying,1","应,Ying,4"
,"缨,Ying,1","莹,Ying,2","萤,Ying,2","营,Ying,2","荧,Ying,2","蝇,Ying,2","迎,Ying,2","赢,Ying,2","盈,Ying,2","影,Ying,3","颖,Ying,3","硬,Ying,4","映,Ying,4","哟,Yo,1","哟,Yo,5"
,"拥,Yong,1","佣,Yong,1","佣,Yong,4","臃,Yong,1","痈,Yong,1","庸,Yong,1","雍,Yong,1","踊,Yong,3","蛹,Yong,3","咏,Yong,3","泳,Yong,3","涌,Chong,1","涌,Yong,3","永,Yong,3","恿,Yong,3"
,"勇,Yong,3","用,Yong,4","幽,You,1","优,You,1","悠,You,1","忧,You,1","尤,You,2","由,You,2","邮,You,2","铀,You,2","犹,You,2","油,You,2","游,You,2","酉,You,3","有,You,3"
,"有,You,4","友,You,3","右,You,4","佑,You,4","釉,You,4","诱,You,4","又,You,4","幼,You,4","迂,Yu,1","淤,Yu,1","于,Yu,2","盂,Yu,2","榆,Yu,2","虞,Yu,2","愚,Yu,2"
,"舆,Yu,2","余,Yu,2","俞,Shu,4","俞,Yu,2","逾,Yu,2","鱼,Yu,2","愉,Yu,2","渝,Yu,2","渔,Yu,2","隅,Yu,2","予,Yu,2","予,Yu,3","娱,Yu,2","雨,Yu,3","雨,Yu,4"
,"与,Yu,2","与,Yu,3","与,Yu,4","屿,Yu,3","禹,Yu,3","宇,Yu,3","语,Yu,3","语,Yu,4","羽,Yu,3","玉,Yu,4","域,Yu,4","芋,Yu,4","郁,Yu,4","吁,Xu,1","吁,Yu,1"
,"吁,Yu,4","遇,Yu,4","喻,Yu,4","峪,Yu,4","御,Yu,4","愈,Yu,4","欲,Yu,4","狱,Yu,4","育,Yo,1","育,Yu,4","誉,Yu,4","浴,Yu,4","寓,Yu,4","裕,Yu,4","预,Yu,4"
,"豫,Yu,4","驭,Yu,4","鸳,Yuan,1","渊,Yuan,1","冤,Yuan,1","元,Yuan,2","垣,Yuan,2","袁,Yuan,2","原,Yuan,2","援,Yuan,2","辕,Yuan,2","园,Yuan,2","员,Yuan,2","员,Yun,2","员,Yun,4"
,"圆,Yuan,2","猿,Yuan,2","源,Yuan,2","缘,Yuan,2","远,Yuan,3","苑,Yuan,4","愿,Yuan,4","怨,Yuan,4","院,Yuan,4","曰,Yue,1","约,Yao,1","约,Yue,1","越,Yue,4","跃,Yue,4","钥,Yao,4"
,"钥,Yue,4","岳,Yue,4","粤,Yue,4","月,Yue,4","悦,Yue,4","阅,Yue,4","耘,Yun,2","云,Yun,2","郧,Yun,2","匀,Yun,2","陨,Yun,3","允,Yun,3","运,Yun,4","蕴,Yun,4","酝,Yun,4"
,"晕,Yun,1","晕,Yun,4","韵,Yun,4","孕,Yun,4","匝,Za,1","砸,Za,2","杂,Za,2","栽,Zai,1","哉,Zai,1","灾,Zai,1","宰,Zai,3","载,Zai,3","载,Zai,4","再,Zai,4","在,Zai,4"
,"咱,Za,2","咱,Zan,2","咱,Zan,5","攒,Cuan,2","攒,Zan,3","暂,Zan,4","赞,Zan,4","赃,Zang,1","脏,Zang,4","葬,Zang,4","遭,Zao,1","糟,Zao,1","凿,Zao,2","凿,Zuo,4","藻,Zao,3"
,"枣,Zao,3","早,Zao,3","澡,Zao,3","蚤,Zao,3","躁,Zao,4","噪,Zao,4","造,Zao,4","皂,Zao,4","灶,Zao,4","燥,Zao,4","责,Ze,2","择,Ze,2","择,Zhai,2","则,Ze,2","泽,Ze,2"
,"贼,Zei,2","怎,Zen,3","增,Zeng,1","憎,Zeng,1","曾,Ceng,2","曾,Zeng,1","赠,Zeng,4","扎,Za,1","扎,Zha,1","扎,Zha,2","喳,Cha,1","喳,Zha,1","渣,Zha,1","札,Zha,2","轧,Ga,2"
,"轧,Ya,4","轧,Zha,2","铡,Zha,2","闸,Zha,2","眨,Zha,3","栅,Shan,1","栅,Zha,4","榨,Zha,4","咋,Za,3","咋,Ze,2","咋,Zha,1","咋,Zha,4","乍,Zha,4","炸,Zha,2","炸,Zha,4"
,"诈,Zha,4","摘,Zhai,1","斋,Zhai,1","宅,Zhai,2","窄,Zhai,3","债,Zhai,4","寨,Zhai,4","瞻,Zhan,1","毡,Zhan,1","詹,Zhan,1","粘,Nian,2","粘,Zhan,1","沾,Zhan,1","盏,Zhan,3","斩,Zhan,3"
,"辗,Zhan,3","崭,Zhan,3","展,Zhan,3","蘸,Zhan,4","栈,Zhan,4","占,Zhan,1","占,Zhan,4","战,Zhan,4","站,Zhan,4","湛,Zhan,4","绽,Zhan,4","樟,Zhang,1","章,Zhang,1","彰,Zhang,1","漳,Zhang,1"
,"张,Zhang,1","掌,Zhang,3","涨,Zhang,3","涨,Zhang,4","杖,Zhang,4","丈,Zhang,4","帐,Zhang,4","账,Zhang,4","仗,Zhang,4","胀,Zhang,4","瘴,Zhang,4","障,Zhang,4","招,Zhao,1","昭,Zhao,1","找,Zhao,3"
,"沼,Zhao,3","赵,Zhao,4","照,Zhao,4","罩,Zhao,4","兆,Zhao,4","肇,Zhao,4","召,Shao,4","召,Zhao,4","遮,Zhe,1","折,She,2","折,Zhe,1","折,Zhe,2","哲,Zhe,2","蛰,Zhe,2","辙,Zhe,2"
,"者,Zhe,3","锗,Zang,1","锗,Zhe,3","蔗,Zhe,4","这,Zhe,4","这,Zhei,4","浙,Zhe,4","珍,Zhen,1","斟,Zhen,1","真,Zhen,1","甄,Zhen,1","砧,Zhen,1","臻,Zhen,1","贞,Zhen,1","针,Zhen,1"
,"侦,Zhen,1","枕,Zhen,3","疹,Zhen,3","诊,Zhen,3","震,Zhen,4","振,Zhen,4","镇,Zhen,4","阵,Zhen,4","蒸,Zheng,1","挣,Zheng,1","挣,Zheng,4","睁,Zheng,1","征,Zheng,1","狰,Zheng,1","争,Zheng,1"
,"怔,Zheng,1","怔,Zheng,4","整,Zheng,3","拯,Zheng,3","正,Zheng,1","正,Zheng,4","政,Zheng,4","帧,Zheng,4","症,Zheng,1","症,Zheng,4","郑,Zheng,4","证,Zheng,4","芝,Zhi,1","枝,Zhi,1","支,Zhi,1"
,"吱,Zhi,1","吱,Zi,1","蜘,Zhi,1","知,Zhi,1","知,Zhi,4","肢,Zhi,1","脂,Zhi,1","汁,Zhi,1","之,Zhi,1","织,Zhi,1","职,Zhi,2","直,Zhi,2","植,Zhi,2","殖,Shi,5","殖,Zhi,2"
,"执,Zhi,2","值,Zhi,2","侄,Zhi,2","址,Zhi,3","指,Zhi,1","指,Zhi,2","指,Zhi,3","止,Zhi,3","趾,Zhi,3","只,Zhi,1","只,Zhi,3","旨,Zhi,3","纸,Zhi,3","志,Zhi,4","挚,Zhi,4"
,"掷,Zhi,1","掷,Zhi,4","至,Zhi,4","致,Zhi,4","置,Zhi,4","帜,Zhi,4","峙,Shi,4","峙,Zhi,4","制,Zhi,4","智,Zhi,4","秩,Zhi,4","稚,Zhi,4","质,Zhi,4","炙,Zhi,4","痔,Zhi,4"
,"滞,Zhi,4","治,Zhi,4","窒,Zhi,4","中,Zhong,1","中,Zhong,4","盅,Zhong,1","忠,Zhong,1","钟,Zhong,1","衷,Zhong,1","终,Zhong,1","种,Chong,2","种,Zhong,3","种,Zhong,4","肿,Zhong,3","重,Chong,2"
,"重,Zhong,4","仲,Zhong,4","众,Zhong,4","舟,Zhou,1","周,Zhou,1","州,Zhou,1","洲,Zhou,1","诌,Zhou,1","粥,Yu,4","粥,Zhou,1","轴,Zhou,2","轴,Zhou,4","肘,Zhou,3","帚,Zhou,3","咒,Zhou,4"
,"皱,Zhou,4","宙,Zhou,4","昼,Zhou,4","骤,Zhou,4","珠,Zhu,1","株,Zhu,1","蛛,Zhu,1","朱,Zhu,1","猪,Zhu,1","诸,Zhu,1","诛,Zhu,1","逐,Zhu,2","竹,Zhu,2","烛,Zhu,2","煮,Zhu,3"
,"拄,Zhu,3","瞩,Zhu,3","嘱,Zhu,3","主,Zhu,3","著,Zhu,4","著,Zhuo,2","柱,Zhu,4","助,Zhu,4","蛀,Zhu,4","贮,Zhu,4","铸,Zhu,4","筑,Zhu,2","筑,Zhu,4","住,Zhu,4","注,Zhu,4"
,"祝,Zhu,4","驻,Zhu,4","抓,Zhua,1","爪,Zhao,3","爪,Zhua,3","拽,Ye,4","拽,Zhuai,1","拽,Zhuai,4","专,Zhuan,1","砖,Zhuan,1","转,Zhuai,3","转,Zhuan,3","转,Zhuan,4","撰,Zhuan,4","赚,Zhuan,4"
,"赚,Zuan,4","篆,Zhuan,4","桩,Zhuang,1","庄,Zhuang,1","装,Zhuang,1","妆,Zhuang,1","撞,Zhuang,4","壮,Zhuang,4","状,Zhuang,4","椎,Chui,2","椎,Zhui,1","锥,Zhui,1","追,Zhui,1","赘,Zhui,4","坠,Zhui,4"
,"缀,Zhui,4","谆,Zhun,1","准,Zhun,3","捉,Zhuo,1","拙,Zhuo,1","卓,Zhuo,1","桌,Zhuo,1","琢,Zhuo,2","琢,Zuo,2","茁,Zhuo,2","酌,Zhuo,2","啄,Zhuo,2","着,Zhao,1","着,Zhao,2","着,Zhe,5"
,"着,Zhuo,2","灼,Zhuo,2","浊,Zhuo,2","兹,Ci,2","兹,Zi,1","咨,Zi,1","资,Zi,1","姿,Zi,1","滋,Zi,1","淄,Zi,1","孜,Zi,1","紫,Zi,3","仔,Zai,3","仔,Zi,1","仔,Zi,3"
,"籽,Zi,3","滓,Zi,3","子,Zi,3","自,Zi,4","渍,Zi,4","字,Zi,4","鬃,Zong,1","棕,Zong,1","踪,Zong,1","宗,Zong,1","综,Zeng,4","综,Zong,1","总,Zong,3","纵,Zong,4","邹,Zou,1"
,"走,Zou,3","奏,Zou,4","揍,Zou,4","租,Zu,1","足,Zu,2","卒,Cu,4","卒,Zu,2","族,Zu,2","祖,Zu,3","诅,Zu,3","阻,Zu,3","组,Zu,3","钻,Zuan,1","钻,Zuan,4","纂,Zuan,3"
,"嘴,Zui,3","醉,Zui,4","最,Zui,4","罪,Zui,4","尊,Zun,1","遵,Zun,1","昨,Zuo,2","左,Zuo,3","佐,Zuo,3","柞,Zha,4","柞,Zuo,4","做,Zuo,4","作,Zuo,1","作,Zuo,2","作,Zuo,4"
,"坐,Zuo,4","座,Zuo,4"
                        };

        #endregion 汉字 拼音 声调 数组 结束

        /// <summary>
        /// 汉字转换拼音
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string HzToPy(string str)
        {
            string py = "";
            for (int i = 0; i < str.Length; i++)
            {
                py += PinYing(str.Substring(i, 1));
            }
            return py;
        }

        private static string PinYing(string str)
        {
            for (int i = 0; i < item_hz.Length; i++)
            {
                string[] item = item_hz[i].Split(',');
                if (str == item[0])
                {
                    str = item[1];
                    break;
                }
            }
            return str;
        }

        /// <summary>
        /// 汉字首字母
        /// </summary>
        /// <returns></returns>
        private static string HzToShZm(string str)
        {
            string zm = "";
            for (int i = 0; i < str.Length; i++)
            {
                zm += ZiMu(str.Substring(i, 1));
            }
            return zm;
        }

        private static string ZiMu(string str)
        {
            for (int i = 0; i < item_hz.Length; i++)
            {
                string[] item = item_hz[i].Split(',');
                if (str == item[0])
                {
                    str = item[1].Substring(0, 1);
                    break;
                }
            }
            return str;
        }

        #endregion
    }
}
