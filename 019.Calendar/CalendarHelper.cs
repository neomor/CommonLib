﻿#region -- 版 本 注 释 --
/****************************************************
* 文 件 名：
* Copyright(c) 王树羽
* CLR 版本: 4.5
* 创 建 人：王树羽
* 电子邮箱：674613047@qq.com
* 官方网站：https://www.cnblogs.com/shuyu
* 创建日期：2018-06-25 
* 文件描述：
******************************************************
* 修 改 人：
* 修改日期：
* 备注描述：
*******************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLib
{
    /// <summary>
    /// 日历帮助类
    /// </summary>
    public class CalendarHelper
    {
        #region 返回本年有多少天

        /// <summary>
        /// 返回本年有多少天
        /// </summary>
        /// <param name="year">年份</param>
        /// <returns>本年的天数</returns>
        public static int GetDaysByYear(int year)
        {
            int cnt = 0;
            if (IsRuYear(year))
            {
                cnt = 366;//闰年多 1 天 即：2 月为 29 天
            }
            else
            {
                cnt = 365;//--非闰年少1天 即：2 月为 28 天
            }
            return cnt;
        }

        /// <summary>
        /// 本年有多少天
        /// </summary>
        /// <param name="dt">日期</param>
        /// <returns>本天在当年的天数</returns>
        public static int GetDaysByYear(DateTime dt)
        {
            return GetDaysByYear(dt.Year);
        }
        #endregion

        #region 返回本月有多少天

        /// <summary>
        /// 本月有多少天
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="Month">月</param>
        /// <returns>天数</returns>
        public static int GetDaysByMonth(int year, int Month)
        {
            int days = 0;
            switch (Month)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    {
                        days = 31;
                        break;
                    }
                case 4:
                case 6:
                case 9:
                case 11:
                    {
                        days = 30;
                        break;
                    }
                case 2:
                    {
                        if (IsRuYear(year))
                        {
                            days = 29;
                        }
                        else
                        {
                            days = 28;
                        }
                        break;
                    }
            }

            return days;
        }

        /// <summary>
        /// 本月有多少天
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetDaysByMonth(DateTime dt)
        {
            int days = GetDaysByMonth(dt.Year, dt.Month);

            return days;
        }
        #endregion 

        #region 返回当前日期的星期名称

        /// <summary>
        /// 返回当前日期的星期名称
        /// </summary>
        /// <param name="dt">日期</param>
        /// <returns>星期名称</returns>
        public static string GetWeekNameOfDay(DateTime dt)
        {
            var str = "";
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    str = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    str = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    str = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    str = "星期四";
                    break;
                case DayOfWeek.Friday:
                    str = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    str = "星期六";
                    break;
                case DayOfWeek.Sunday:
                    str = "星期日";
                    break;
            }
            return str;
        }
        #endregion

        #region 返回当前日期的星期编号

        /// <summary>
        /// 返回当前日期的星期编号
        /// </summary>
        /// <param name="dt">日期</param>
        /// <returns>星期数字编号</returns>
        public static int GetWeekNumberOfDay(DateTime dt)
        {
            return (int)dt.DayOfWeek;
        }
        #endregion

        #region 判断当前日期所属的年份是否是闰年，私有函数

        /// <summary>
        /// 判断当前日期所属的年份是否是闰年，私有函数
        /// </summary>
        /// <param name="dt">日期</param>
        /// <returns>是闰年：True ，不是闰年：False</returns>
        public static bool IsRuYear(DateTime dt)
        {
            return IsRuYear(dt.Year);
        }

        /// <summary>
        /// 判断当前年份是否是闰年
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public static bool IsRuYear(int year)
        {
            if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
